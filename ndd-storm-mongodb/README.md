# NDD Storm Mongo

NDD Storm MongoDB provides Trident states with a MongoDB backend. Released under the [LGPL](https://www.gnu.org/licenses/lgpl.html).

## Usage

For example:

```java
// output fields: group_by_A, group_by_B, value_1, value_2
IRichSpout spout = ...

StateFactory stateFactory =
    MongoStateFactory.nonTransactional(
    // or MongoStateFactory.transactional(
    // or MongoStateFactory.opaque(
        new MongoStateSettings(
            "mongodb://<host>:<port>/<db>.<collection>?<options>",
            new String[] { "group_by_A", "group_by_B" },
            new String[] { "sum_1", "sum_2" }));

TridentTopology topology = new TridentTopology();
topology.newStream("my_stream", spout)
        .groupBy(new Fields("group_by_A", "group_by_B"))
        .persistentAggregate(
                stateFactory,
                new Fields("value_1", "value_2"),
                new SumSum(),
                new Fields("sumsum"));
```

Using the **non transactional** state, a Mongo document may look like:

```json
{
  "_id": {
    "group_by_A": "value_A",
    "group_by_B": "value_B"
  },
  "value": {
    "key_1": "value_1",
    "key_2": "value_2"
  }
}
```

Using the **transactional** state, a Mongo document may look like:

```json
{
  "_id": {
    "group_by_A": "value_A",
    "group_by_B": "value_B"
  },
  "value": {
    "key_1": "value_1",
    "key_2": "value_2"
  },
  "txid": 123
}
```

Using the **opaque** state, a Mongo document may look like:

```json
{
  "_id": {
    "group_by_A": "value_A",
    "group_by_B": "value_B"
  },
  "previous": {
    "key_1": "old_value_1",
    "key_2": "old_value_2"
  },
  "value": {
    "key_1": "current_value_1",
    "key_2": "current_value_2"
  },
  "txid": 123
}
```

## Setup

Using Maven:

```xml
<dependency>
  <groupId>name.didier.david.storm</groupId>
  <artifactId>ndd-storm-mongo</artifactId>
  <version>X.Y.Z</version>
  <scope>compile</scope>
</dependency>
```

## Reports

Maven reports are available at [https://ndd-java.bitbucket.org/ndd-storm-mongodb/project-reports.html](https://ndd-java.bitbucket.org/ndd-storm-mongodb/project-reports.html)

## About

Thanks to the [Apache Storm community](https://storm.incubator.apache.org/community.html) !

Widely inspired by:

- [https://github.com/eldenbishop/trident-mongodb](https://github.com/eldenbishop/trident-mongodb)
- [https://github.com/wilbinsc/storm-mongo](https://github.com/wilbinsc/storm-mongo)

Copyright David DIDIER 2014
