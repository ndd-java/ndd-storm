package name.didier.david.storm.mongodb;

import static java.util.Arrays.asList;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotBlank;
import static name.didier.david.check4j.api.ConciseCheckers.checkNotEmpty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.common.base.MoreObjects;
import com.google.common.base.MoreObjects.ToStringHelper;
import com.mongodb.MongoClientURI;

import storm.trident.state.map.CachedMap;
import storm.trident.state.map.IBackingMap;

/**
 * Settings for {@link MongoState}. The MongoDB URI is a string since {@link MongoClientURI} is not {@link Serializable}
 * . Format details can be found in the Java documentation. Key fields are the names of the fields composing the DB key.
 * Value fields are the names of the fields composing the DB record.
 *
 * <p>
 * For example if:
 *
 * <pre>
 * keyField = { "user", "age" }
 * valueField = { "sum", "average", "some_other_value" }
 * </pre>
 *
 * </p>
 * <p>
 * a DB record may look like:
 *
 * <pre>
 * {
 *     "_id" : {
 *         "user" : "john",
 *         "age" : 33
 *     },
 *     "value" : {
 *         "sum" : 1000,
 *         "average" : 123,
 *         "some_other_value" : 12345
 *     }
 * }
 * </pre>
 *
 * </p>
 *
 * @author ddidier
 */
public class MongoStateSettings
        implements Serializable {

    /** The default size of the batch reading data in {@link IBackingMap#multiGet(List)}. */
    public static final int DEFAULT_BULK_SIZE = Integer.MAX_VALUE;
    /** The default size of the cache managed by {@link CachedMap}. */
    public static final int DEFAULT_CACHE_SIZE = 10_000;

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The MongoDB URI. */
    private final String uri;
    /** The names of the fields composing the DB key. */
    private final List<String> keyFields;
    /** The names of the fields composing the DB record. */
    private final List<String> valueFields;
    /** The size of the batch reading data in {@link IBackingMap#multiGet(List)}. */
    private int bulkSize = DEFAULT_BULK_SIZE;
    /** The size of the cache managed by {@link CachedMap}. */
    private int cacheSize = DEFAULT_CACHE_SIZE;

    /**
     * Constructor using arrays.
     *
     * @param uri the MongoDB URI.
     * @param keyFields the names of the fields composing the DB key.
     * @param valueFields the names of the fields composing the DB record.
     */
    public MongoStateSettings(String uri, String[] keyFields, String[] valueFields) {
        this(uri, asList(keyFields), asList(valueFields));
    }

    /**
     * Constructor using lists.
     *
     * @param uri the MongoDB URI.
     * @param keyFields the names of the fields composing the DB key.
     * @param valueFields the names of the fields composing the DB record.
     */
    public MongoStateSettings(String uri, List<String> keyFields, List<String> valueFields) {
        this.uri = checkNotBlank(uri, "uri");
        this.keyFields = new ArrayList<>(checkNotEmpty(keyFields, "keyFields"));
        this.valueFields = new ArrayList<>(checkNotEmpty(valueFields, "valueFields"));
    }

    /**
     * @return the MongoDB URI.
     */
    public String getUri() {
        return uri;
    }

    /**
     * @return the names of the fields composing the DB key.
     */
    public List<String> getKeyFields() {
        return keyFields;
    }

    /**
     * @return the names of the fields composing the DB record.
     */
    public List<String> getValueFields() {
        return valueFields;
    }

    /**
     * @return the size of the batch reading data.
     */
    public int getBulkSize() {
        return bulkSize;
    }

    /**
     * @param bulkSize the size of the batch reading data.
     */
    public void setBulkSize(int bulkSize) {
        this.bulkSize = bulkSize;
    }

    /**
     * @return the size of the cache.
     */
    public int getCacheSize() {
        return cacheSize;
    }

    /**
     * @param cacheSize the size of the cache.
     */
    public void setCacheSize(int cacheSize) {
        this.cacheSize = cacheSize;
    }

    @Override
    public String toString() {
        ToStringHelper builder = MoreObjects.toStringHelper(this);
        builder.add("uri", uri);
        builder.add("keyFields", keyFields);
        builder.add("valueFields", valueFields);
        builder.add("bulkSize", bulkSize);
        builder.add("cacheSize", cacheSize);
        return builder.toString();
    }
}
