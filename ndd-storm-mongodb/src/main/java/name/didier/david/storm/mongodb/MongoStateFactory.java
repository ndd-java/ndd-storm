package name.didier.david.storm.mongodb;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;
import static storm.trident.state.StateType.NON_TRANSACTIONAL;
import static storm.trident.state.StateType.OPAQUE;
import static storm.trident.state.StateType.TRANSACTIONAL;

import java.util.Map;

import backtype.storm.task.IMetricsContext;
import storm.trident.state.State;
import storm.trident.state.StateFactory;
import storm.trident.state.StateType;
import storm.trident.state.map.CachedMap;
import storm.trident.state.map.NonTransactionalMap;
import storm.trident.state.map.OpaqueMap;
import storm.trident.state.map.TransactionalMap;

/**
 * A mongoFactory creating {@link MongoState} using {@link MongoStateSettings}.
 * 
 * @author ddidier
 */
public class MongoStateFactory
        implements StateFactory {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The type of the Trident state. */
    private final StateType stateType;
    /** The settings the Trident state. */
    private final MongoStateSettings settings;

    /**
     * Type and settings are passed to the constructor.
     * 
     * @param stateType the type of the Trident state.
     * @param settings the settings of the {@link MongoState}.
     */
    protected MongoStateFactory(StateType stateType, MongoStateSettings settings) {
        this.stateType = checkNotNull(stateType, "stateType");
        this.settings = checkNotNull(settings, "settings");
    }

    @Override
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public State makeState(Map stormConfiguration, IMetricsContext context, int partitionIndex, int partitionNumber) {
        MongoState mongoState = new MongoState(stateType, settings);
        CachedMap cachedState = new CachedMap(mongoState, settings.getCacheSize());
        State state = null;

        switch (stateType) {
        case NON_TRANSACTIONAL:
            state = NonTransactionalMap.build(cachedState);
            break;
        case TRANSACTIONAL:
            state = TransactionalMap.build(cachedState);
            break;
        case OPAQUE:
            state = OpaqueMap.build(cachedState);
            break;
        default:
            throw new IllegalArgumentException("Unsupported state type: " + stateType);
        }

        return state;
    }

    /**
     * @param settings the settings passed to the state.
     * @return a mongoFactory creating a non transactional cached {@link MongoState}.
     */
    public static StateFactory nonTransactional(MongoStateSettings settings) {
        return newFactory(NON_TRANSACTIONAL, settings);
    }

    /**
     * @param settings the settings passed to the state.
     * @return a mongoFactory creating a transactional cached {@link MongoState}.
     */
    public static StateFactory transactional(MongoStateSettings settings) {
        return newFactory(TRANSACTIONAL, settings);
    }

    /**
     * @param settings the settings passed to the state.
     * @return a mongoFactory creating an opaque cached {@link MongoState}.
     */
    public static StateFactory opaque(MongoStateSettings settings) {
        return newFactory(OPAQUE, settings);
    }

    /**
     * @param stateType the type of the state.
     * @param settings the settings passed to the state.
     * @return a mongoFactory creating a cached {@link MongoState} of the specified type.
     */
    public static MongoStateFactory newFactory(StateType stateType, MongoStateSettings settings) {
        return new MongoStateFactory(stateType, settings);
    }
}
