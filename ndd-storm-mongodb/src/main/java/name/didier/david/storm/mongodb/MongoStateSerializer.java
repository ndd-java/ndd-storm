package name.didier.david.storm.mongodb;

import java.io.Serializable;

import com.mongodb.DBObject;

/**
 * Serialize an object to a MongoDB document back and forth.
 *
 * @author ddidier
 * @param <T> the type of the object to serialize.
 */
public interface MongoStateSerializer<T>
        extends Serializable {

    /**
     * Serialize an object to a MongoDB document.
     *
     * @param id the ID of the document.
     * @param value the object to convert to document.
     * @return the document representing the object.
     */
    DBObject serialize(DBObject id, T value);

    /**
     * Deserialize a MongoDB document into an object.
     *
     * @param document the document to convert to an object.
     * @return the object representing the document.
     */
    T deserialize(DBObject document);
}
