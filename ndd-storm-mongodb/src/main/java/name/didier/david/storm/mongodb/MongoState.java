package name.didier.david.storm.mongodb;

import static com.google.common.collect.Lists.partition;
import static com.google.common.collect.Lists.transform;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Function;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import storm.trident.state.StateType;
import storm.trident.state.map.IBackingMap;

/**
 * A Trident state using a MongoDB backend.
 *
 * @author ddidier
 * @param <T> kind of unused...
 */
public class MongoState<T>
        implements IBackingMap<T> {

    /** The name of an ID field in MongoDB. */
    private static final String ID = "_id";

    /** The associated logger. */
    private static final Logger logger = LoggerFactory.getLogger(MongoState.class);

    /** The settings the Trident state. */
    private final MongoStateSettings settings;
    /** Know how to serialize document according to a given state type. */
    private final MongoStateSerializer<T> serializer;
    /** The MongoDB collection backing this state. */
    private final DBCollection collection;
    /** Convert a list of objects into a document ID. */
    private final Function<List<Object>, DBObject> converter;

    /**
     * Use {@link MongoStateFactory} instead.
     *
     * @param stateType the type of the Trident state.
     * @param settings the settings the Trident state.
     */
    protected MongoState(StateType stateType, MongoStateSettings settings) {
        checkNotNull(stateType, "stateType");
        this.settings = checkNotNull(settings, "settings");
        this.serializer = createSerializer(stateType, settings);
        this.collection = createCollection(stateType, settings);
        this.converter = createConverter(stateType, settings);
    }

    @Override
    public List<T> multiGet(List<List<Object>> keys) {
        // convert all keys into MongoDB ID documents
        final List<DBObject> requestedIds = transform(keys, converter);

        // create an ID/document map to figure out which keys are present
        Map<DBObject, DBObject> foundDocumentByIds = new HashMap<>(requestedIds.size());
        // retrieve documents by bulk of the specified size
        List<List<DBObject>> partitionsOfIds = partition(requestedIds, settings.getBulkSize());

        for (List<DBObject> partitionOfIds : partitionsOfIds) {
            // retrieve a single partition of documents
            List<DBObject> partitionOfFoundDocuments = batchGet(partitionOfIds);

            // map the retrieved documents by ID
            for (DBObject foundDocument : partitionOfFoundDocuments) {
                foundDocumentByIds.put((DBObject) foundDocument.get(ID), foundDocument);
            }
        }

        // some requested documents may not have been found, but we need to return a corresponding tuple anyway
        List<T> documents = new ArrayList<>(requestedIds.size());

        // for each requested ID, deserialize the document if present, return null otherwise
        for (DBObject requestedId : requestedIds) {
            DBObject document = foundDocumentByIds.get(requestedId);

            if (document == null) {
                documents.add(null);
            } else {
                documents.add(serializer.deserialize(document));
            }
        }

        return documents;
    }

    @Override
    public void multiPut(List<List<Object>> keys, List<T> values) {
        // BulkWriteOperation bulk = collection.initializeOrderedBulkOperation();

        Iterator<List<Object>> itK = keys.iterator();
        Iterator<T> itV = values.iterator();

        while (itK.hasNext()) {
            List<Object> key = itK.next();
            T value = itV.next();
            DBObject documentId = converter.apply(key);
            DBObject document = serializer.serialize(documentId, value);
            collection.update(new BasicDBObject(ID, documentId), document, true, false);
            // bulk.find(new BasicDBObject("_id", id)).upsert().update(document);
        }

        // bulk.execute();
        // bulk.execute(WriteConcern.ACKNOWLEDGED);
        logger.debug("{} keys flushed", keys.size());
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @param stateType the type of the Trident state.
     * @param stateSettings the settings the Trident state.
     *
     * @return a new serializer matching the specified state type.
     */
    protected MongoStateSerializer<T> createSerializer(StateType stateType, MongoStateSettings stateSettings) {
        // CSOFF: ReturnCountCheck -1
        switch (stateType) {
        case NON_TRANSACTIONAL:
            return new MongoStateNonTransactionalSerializer<>(stateSettings);
        case TRANSACTIONAL:
            return new MongoStateTransactionalSerializer<>(stateSettings);
        case OPAQUE:
            return new MongoStateOpaqueSerializer<>(stateSettings);
        default:
            throw new IllegalArgumentException("Unsupported state type: " + stateType);
        }
    }

    /**
     * @param stateType the type of the Trident state.
     * @param stateSettings the settings the Trident state.
     *
     * @return the MongoDB collection specified in the settings.
     */
    protected DBCollection createCollection(StateType stateType, MongoStateSettings stateSettings) {
        try {
            MongoClientURI mongoUri = new MongoClientURI(stateSettings.getUri());
            MongoClient mongoClient = new MongoClient(mongoUri);
            DB mongoDb = mongoClient.getDB(mongoUri.getDatabase());
            return mongoDb.getCollection(mongoUri.getCollection());
        } catch (UnknownHostException e) {
            // TODO retry ?
            throw new IllegalArgumentException("Error while initializing MongoDB connection", e);
        }
    }

    /**
     * Returns a new function converting a list of objects into a document, using the key fields for ordering. For
     * example:
     *
     * <pre>
     * if settings.keyFields = { "first_name", "surname" }
     * and given values is the list [ "John", "DOE" ]
     * then the document is { "first_name" : "John", "surname" : "DOE" }
     * </pre>
     *
     * @param stateType the type of the Trident state.
     * @param stateSettings the settings the Trident state.
     *
     * @return a new function converting a list of objects into a document.
     */
    protected Function<List<Object>, DBObject> createConverter(StateType stateType, MongoStateSettings stateSettings) {
        return new MongoStateIdConverter(stateSettings.getKeyFields());
    }

    /**
     * @param ids the IDs of the documents to retrieve.
     * @return all the given documents in one batch request.
     */
    protected List<DBObject> batchGet(List<DBObject> ids) {
        BasicDBObject request = new BasicDBObject(ID, new BasicDBObject("$in", ids));
        try (DBCursor cursor = collection.find(request)) {
            return cursor.toArray();
        }
    }
}
