package name.didier.david.storm.mongodb;

import static java.lang.String.format;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotEmpty;

import java.util.Iterator;
import java.util.List;

import com.google.common.base.Function;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Convert a list of objects into a document, using the given names for ordering. For example:
 *
 * <pre>
 * if names = { "first_name", "surname" }
 * and values is the list [ "John", "DOE" ]
 * then the document is { "first_name" : "John", "surname" : "DOE" }
 * </pre>
 *
 * @author ddidier
 */
public class MongoStateIdConverter
        implements Function<List<Object>, DBObject> {

    /** The names of the document ID fields. */
    private final List<String> names;

    /**
     * Default constructor.
     *
     * @param names the names of the document ID fields.
     */
    public MongoStateIdConverter(List<String> names) {
        super();
        this.names = checkNotEmpty(names, "names");
    }

    @Override
    public DBObject apply(List<Object> values) {
        if (names.size() != values.size()) {
            throw new IllegalArgumentException(format(
                    "Names and values must have the same size but %n  names = %s %nand%n  values = %s", names, values));
        }

        BasicDBObject id = new BasicDBObject(values.size());
        Iterator<String> itN = names.iterator();
        Iterator<Object> itV = values.iterator();
        while (itN.hasNext()) {
            id.put(itN.next(), itV.next());
        }

        return id;
    }
}
