package name.didier.david.storm.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import storm.trident.state.OpaqueValue;

/**
 * Serialize an object to a MongoDB document back and forth in a format suitable for an opaque Trident state. The format
 * of the document is:
 *
 * <pre>
 * {
 *   "_id": {
 *     "id_k1": "id_v1",
 *     "id_k2": "id_v2"
 *   },
 *   "previous": {
 *     "k1": "old_v1",
 *     "k2": "old_v2"
 *   },
 *   "value": {
 *     "k1": "current_v1",
 *     "k2": "current_v2"
 *   },
 *   "txid": 123
 * }
 * </pre>
 *
 * @author ddidier
 * @param <T> the type of the object to serialize.
 */
public class MongoStateOpaqueSerializer<T>
        extends AbstractMongoStateSerializer<T> {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /**
     * @param settings the settings of the Trident state.
     */
    public MongoStateOpaqueSerializer(MongoStateSettings settings) {
        super(settings);
    }

    @Override
    public DBObject serialize(DBObject id, Object value) {
        OpaqueValue<?> opaqueValue = (OpaqueValue<?>) value;
        BasicDBObject document = new BasicDBObject(ID_KEY, id);
        document.append(VALUE_KEY, valueToDocument(opaqueValue.getCurr()));
        document.append(TX_ID_KEY, opaqueValue.getCurrTxid());
        document.append(PREVIOUS_KEY, valueToDocument(opaqueValue.getPrev()));
        return document;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T deserialize(DBObject document) {
        Long txid = (Long) document.get(TX_ID_KEY);
        Object value = documentToValue((DBObject) document.get(VALUE_KEY));
        Object previousValue = documentToValue((DBObject) document.get(PREVIOUS_KEY));
        return (T) new OpaqueValue<>(txid, value, previousValue);
    }
}
