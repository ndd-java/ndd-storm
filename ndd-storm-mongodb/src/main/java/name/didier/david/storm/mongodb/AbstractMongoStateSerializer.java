package name.didier.david.storm.mongodb;

import static java.lang.String.format;

import static com.google.common.collect.Lists.transform;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Utilities methods for serializing a tuple to a MongoDB document back and forth.
 *
 * @author ddidier
 * @param <T> the type of the object to serialize.
 */
public abstract class AbstractMongoStateSerializer<T>
        implements MongoStateSerializer<T> {

    /** The key indexing the ID in the DB. */
    public static final String ID_KEY = "_id";
    /** The key indexing the value in the DB. */
    public static final String VALUE_KEY = "value";
    /** The key indexing the transaction ID in the DB. */
    public static final String TX_ID_KEY = "txid";
    /** The key indexing the previous value in the DB. */
    public static final String PREVIOUS_KEY = "previous";

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The settings of the state. */
    protected final MongoStateSettings settings;
    /** The names of the fields composing the DB record. */
    protected List<String> valueFields;

    /**
     * Default constructor.
     *
     * @param settings the settings of the Trident state.
     */
    protected AbstractMongoStateSerializer(MongoStateSettings settings) {
        super();
        this.settings = checkNotNull(settings, "settings");
        this.valueFields = settings.getValueFields();
    }

    /**
     * Convert a document to a value tuple (list of objects) ordered based on value fields. If there is a single value
     * field, we return the object itself rather than a singleton list. For example:
     *
     * <pre>
     * if valueFields = { "name" }
     * and document = { "name" : "john" }
     * then documentToValue is the string "john"
     * 
     * if valueFields = { "name", "age" }
     * and document = { "age" : 33, "name" : "john" } // note the order
     * then documentToValue is the list [ "john", 33 ]
     * </pre>
     *
     * @param document the sub-document to convert.
     * @return an object or a list of objects.
     */
    protected Object documentToValue(final DBObject document) {
        // CSOFF: ReturnCountCheck -1
        if (document == null) {
            return null;
        }

        List<Object> values = transform(valueFields, new Function<String, Object>() {
            @Override
            public Object apply(final String field) {
                return document.get(field);
            }
        });

        if (values.size() == 1) {
            return values.get(0);
        }

        return values;
    }

    /**
     * Convert a value to a document. If there's only one value field, we map the value field name directly to the
     * value, otherwise we take the value fields list as keys (the value is assumed to be a list of equal length) and
     * construct a map out of it. For example:
     *
     * <pre>
     * if valueFields = { "name" }
     * and value is the string "john"
     * then valueToDocument is the document { "name" : "john" }
     * 
     * if valueFields = { "name", "age" }
     * and value is the list [ "john", 33 ]
     * then valueToDocument is the document { "name" : "john", "age" : 33 }
     * </pre>
     *
     * @param value an object or a list of objects.
     * @return the document.
     */
    protected DBObject valueToDocument(Object value) {
        // CSOFF: ReturnCountCheck -1
        // if (value == null) {
        // return null;
        // }

        if (valueFields.size() == 1) {
            return valueToDocument(valueFields.get(0), value);
        }

        if (value == null) {
            return valueToDocument(valueFields);
        }

        return valueToDocument(valueFields, checkList(value));
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * @param value the object to cast.
     * @return the given argument casted as {@link List}.
     * @throws IllegalArgumentException if the given argument is not a {@link List}.
     */
    @SuppressWarnings("unchecked")
    private List<Object> checkList(Object value) {
        if (value instanceof List) {
            return (List<Object>) value;
        }
        throw new IllegalArgumentException(format("Expected value to be a '%s' but was a '%s'", List.class.getName(),
                value.getClass().getName()));
    }

    /**
     * @param fieldName the name of the document single field.
     * @param fieldValue the value of the document single field.
     * @return a new document with a single field.
     */
    private DBObject valueToDocument(String fieldName, Object fieldValue) {
        return new BasicDBObject(fieldName, fieldValue);
    }

    /**
     * @param fieldNames the names of the document fields.
     * @param fieldValues the values of the document fields.
     * @return a new document with the given field name/value pairs.
     */
    private DBObject valueToDocument(List<String> fieldNames, List<Object> fieldValues) {
        if (fieldNames.size() != fieldValues.size()) {
            throw new IllegalArgumentException(format(
                    "Names and values must have the same size but %n  fieldNames = %s %nand%n  fieldValues = %s",
                    fieldNames, fieldValues));
        }

        DBObject document = new BasicDBObject(fieldNames.size());
        Iterator<Object> itV = fieldValues.iterator();
        Iterator<String> itN = fieldNames.iterator();

        while (itV.hasNext()) {
            document.put(itN.next(), itV.next());
        }

        return document;
    }

    /**
     * @param fieldNames the names of the document fields.
     * @return a new document with all the given fields set to null.
     */
    private DBObject valueToDocument(List<String> fieldNames) {
        Map<String, Object> map = new HashMap<>();
        Iterator<String> itN = fieldNames.iterator();

        while (itN.hasNext()) {
            map.put(itN.next(), null);
        }

        return new BasicDBObject(map);
    }
}
