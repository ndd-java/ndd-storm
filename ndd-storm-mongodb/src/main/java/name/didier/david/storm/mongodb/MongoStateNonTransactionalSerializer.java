package name.didier.david.storm.mongodb;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

/**
 * Serialize an object to a MongoDB document back and forth in a format suitable for a non transactional Trident state.
 * The format of the document is:
 *
 * <pre>
 * {
 *   "_id": {
 *     "id_k1": "id_v1",
 *     "id_k2": "id_v2"
 *   },
 *   "value": {
 *     "k1": "v1",
 *     "k2": "v2"
 *   }
 * }
 * </pre>
 *
 * @author ddidier
 * @param <T> the type of the object to serialize.
 */
public class MongoStateNonTransactionalSerializer<T>
        extends AbstractMongoStateSerializer<T> {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /**
     * @param settings the settings of the Trident state.
     */
    public MongoStateNonTransactionalSerializer(MongoStateSettings settings) {
        super(settings);
    }

    @Override
    public DBObject serialize(DBObject id, Object value) {
        BasicDBObject document = new BasicDBObject(ID_KEY, id);
        return document.append(VALUE_KEY, valueToDocument(value));
    }

    @Override
    @SuppressWarnings("unchecked")
    public T deserialize(DBObject document) {
        return (T) documentToValue((DBObject) document.get(VALUE_KEY));
    }
}
