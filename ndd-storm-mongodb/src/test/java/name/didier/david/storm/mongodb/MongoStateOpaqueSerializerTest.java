package name.didier.david.storm.mongodb;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

import static com.google.common.collect.ImmutableMap.of;
import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.storm.mongodb.AbstractMongoStateSerializer.ID_KEY;
import static name.didier.david.storm.mongodb.AbstractMongoStateSerializer.PREVIOUS_KEY;
import static name.didier.david.storm.mongodb.AbstractMongoStateSerializer.TX_ID_KEY;
import static name.didier.david.storm.mongodb.AbstractMongoStateSerializer.VALUE_KEY;
import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.util.List;
import java.util.Map;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import name.didier.david.test4j.testng.TestNgMockitoListener;
import storm.trident.state.OpaqueValue;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public class MongoStateOpaqueSerializerTest
        extends AbstractMongoStateSerializerTestCase {

    @SuppressWarnings("unchecked")
    public void should_serialize_values_without_previous_version() {
        DBObject id = new BasicDBObject(ID_K1, ID_V1);
        Object value = new OpaqueValue<Object>(TX_ID, newArrayList(V1, V2));
        List<String> valueFields = newArrayList(K1, K2);

        Map<String, ?> dbo = serialize(id, value, valueFields);
        assertThat(dbo.keySet()).containsOnly(ID_KEY, VALUE_KEY, PREVIOUS_KEY, TX_ID_KEY);
        assertThat((Map<String, ?>) dbo.get(ID_KEY)).containsOnly(entry(ID_K1, ID_V1));
        assertThat((Map<String, ?>) dbo.get(VALUE_KEY)).containsOnly(entry(K1, V1), entry(K2, V2));
        assertThat((Map<String, ?>) dbo.get(PREVIOUS_KEY)).containsOnly(entry(K1, null), entry(K2, null));
        assertThat(dbo.get(TX_ID_KEY)).isEqualTo(TX_ID);
    }

    @SuppressWarnings("unchecked")
    public void should_serialize_values_with_previous_version() {
        DBObject id = new BasicDBObject(ID_K1, ID_V1);
        Object value = new OpaqueValue<Object>(TX_ID, newArrayList(V1, V2), newArrayList(V3, V4));
        List<String> valueFields = newArrayList(K1, K2);

        Map<String, ?> dbo = serialize(id, value, valueFields);
        assertThat(dbo.keySet()).containsOnly(ID_KEY, VALUE_KEY, PREVIOUS_KEY, TX_ID_KEY);
        assertThat((Map<String, ?>) dbo.get(ID_KEY)).containsOnly(entry(ID_K1, ID_V1));
        assertThat((Map<String, ?>) dbo.get(VALUE_KEY)).containsOnly(entry(K1, V1), entry(K2, V2));
        assertThat((Map<String, ?>) dbo.get(PREVIOUS_KEY)).containsOnly(entry(K1, V3), entry(K2, V4));
        assertThat(dbo.get(TX_ID_KEY)).isEqualTo(TX_ID);
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void should_deserialize_values_without_previous_version() {
        DBObject id = new BasicDBObject(of(ID_K1, ID_V1));
        DBObject value = new BasicDBObject(of(K1, V1, K2, V2));
        DBObject previous = new BasicDBObject();
        previous.put(K1, null);
        previous.put(K2, null);
        DBObject document = new BasicDBObject(
                of(ID_KEY, id, VALUE_KEY, value, TX_ID_KEY, TX_ID, PREVIOUS_KEY, previous));
        List<String> valueFields = newArrayList(K1, K2);

        OpaqueValue<?> oValue = (OpaqueValue<?>) deserialize(document, valueFields);
        assertThat(oValue.getCurrTxid()).isEqualTo(TX_ID);
        assertThat(oValue.getCurr()).isEqualTo(newArrayList(V1, V2));
        assertThat(oValue.getPrev()).isEqualTo(newArrayList(null, null));
    }

    public void should_deserialize_values_with_previous_version() {
        DBObject id = new BasicDBObject(of(ID_K1, ID_V1));
        DBObject value = new BasicDBObject(of(K1, V1, K2, V2));
        DBObject previous = new BasicDBObject(of(K1, V3, K2, V4));
        DBObject document = new BasicDBObject(
                of(ID_KEY, id, VALUE_KEY, value, TX_ID_KEY, TX_ID, PREVIOUS_KEY, previous));
        List<String> valueFields = newArrayList(K1, K2);

        OpaqueValue<?> oValue = (OpaqueValue<?>) deserialize(document, valueFields);
        assertThat(oValue.getCurrTxid()).isEqualTo(TX_ID);
        assertThat(oValue.getCurr()).isEqualTo(newArrayList(V1, V2));
        assertThat(oValue.getPrev()).isEqualTo(newArrayList(V3, V4));
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected MongoStateSerializer<Object> newSerializer(MongoStateSettings stateSettings) {
        return new MongoStateOpaqueSerializer<>(stateSettings);
    }
}
