package name.didier.david.storm.mongodb;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.mock;

import static name.didier.david.test4j.testng.TestNgGroups.SLOW;
import static name.didier.david.test4j.testng.TestNgGroups.UNIT;
import static storm.trident.state.StateType.NON_TRANSACTIONAL;

import java.util.Map;

import org.mockito.Mock;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import backtype.storm.task.IMetricsContext;
import name.didier.david.test4j.testng.TestNgMockitoListener;
import storm.trident.state.State;
import storm.trident.state.StateType;
import storm.trident.state.map.NonTransactionalMap;
import storm.trident.state.map.OpaqueMap;
import storm.trident.state.map.TransactionalMap;

@Test(groups = { UNIT, SLOW })
@Listeners(TestNgMockitoListener.class)
public class MongoStateFactoryTest
        extends AbstractMongoDbTestCase {

    @Mock
    private Map<?, ?> tridentSettings;
    @Mock
    private IMetricsContext stormContext;

    // -----------------------------------------------------------------------------------------------------------------

    public void constructor_should_reject_null_state() {
        try {
            factory(null, mock(MongoStateSettings.class));
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("stateType");
        }
    }

    public void constructor_should_reject_null_settings() {
        try {
            factory(NON_TRANSACTIONAL, null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("settings");
        }
    }

    public void nonTransactional_should_return_a_non_transactional_map() {
        MongoStateSettings settings = settings("non_transactional");
        State state = MongoStateFactory.nonTransactional(settings).makeState(tridentSettings, stormContext, 0, 0);
        assertThat(state).isInstanceOf(NonTransactionalMap.class);
    }

    public void transactional_should_return_a_transactional_map() {
        MongoStateSettings settings = settings("transactional");
        State state = MongoStateFactory.transactional(settings).makeState(tridentSettings, stormContext, 0, 0);
        assertThat(state).isInstanceOf(TransactionalMap.class);
    }

    public void opaque_should_return_a_opaque_map() {
        MongoStateSettings settings = settings("opaque");
        State state = MongoStateFactory.opaque(settings).makeState(tridentSettings, stormContext, 0, 0);
        assertThat(state).isInstanceOf(OpaqueMap.class);
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static MongoStateFactory factory(StateType stateType, MongoStateSettings settings) {
        return new MongoStateFactory(stateType, settings);
    }

    private static MongoStateSettings settings(String collectionName) {
        return new MongoStateSettings(mongoUri(collectionName), new String[] { "k1" }, new String[] { "v1" });
    }
}
