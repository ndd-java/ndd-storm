package name.didier.david.storm.mongodb;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;
import static storm.trident.state.StateType.NON_TRANSACTIONAL;

import org.mockito.Mock;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgMockitoListener;
import storm.trident.state.StateType;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public class MongoStateTest {

    @Mock
    private MongoStateSettings settings;

    // -----------------------------------------------------------------------------------------------------------------

    public void constructor_should_reject_null_type() {
        try {
            newState(null, settings);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("stateType");
        }
    }

    public void constructor_should_reject_null_settings() {
        try {
            newState(NON_TRANSACTIONAL, null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("settings");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static MongoState<?> newState(StateType stateType, MongoStateSettings settings) {
        return new MongoState<>(stateType, settings);
    }
}
