package name.didier.david.storm.mongodb;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.when;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.util.List;
import java.util.Map;

import org.mockito.Mock;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.mongodb.DBObject;

import name.didier.david.test4j.testng.TestNgMockitoListener;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public abstract class AbstractMongoStateSerializerTestCase {

    protected static final String ID_K1 = "id_k1";
    protected static final String ID_K2 = "id_k2";

    protected static final String ID_V1 = "id_v1";
    protected static final String ID_V2 = "id_v2";

    protected static final String K1 = "k1";
    protected static final String K2 = "k2";
    protected static final String K3 = "k3";
    protected static final String K4 = "k4";

    protected static final String V1 = "v1";
    protected static final String V2 = "v2";
    protected static final String V3 = "v3";
    protected static final String V4 = "v4";

    protected static final long TX_ID = 123L;

    @Mock
    protected MongoStateSettings settings;

    // -----------------------------------------------------------------------------------------------------------------

    public void constructor_should_reject_null_settings() {
        try {
            newSerializer(null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("settings");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    protected abstract MongoStateSerializer<Object> newSerializer(MongoStateSettings stateSettings);

    protected Map<String, ?> serialize(DBObject id, Object value, List<String> valueFields) {
        when(settings.getValueFields()).thenReturn(valueFields);
        return newSerializer(settings).serialize(id, value).toMap();
    }

    protected Object deserialize(DBObject document, List<String> valueFields) {
        when(settings.getValueFields()).thenReturn(valueFields);
        return newSerializer(settings).deserialize(document);
    }

    protected String toString(Map<String, ?> dbo) {
        return dbo.toString().replaceAll("\\s", "").replace("\"", "'");
    }
}
