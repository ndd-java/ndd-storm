package name.didier.david.storm.mongodb;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import com.mongodb.DBObject;

@Test(groups = UNIT)
public class MongoStateIdConverterTest {

    private static final String N1 = "n1";
    private static final String N2 = "n2";

    private static final Object V1 = "v1";
    private static final Object V2 = "v2";

    public void constructor_should_reject_empty_names() {
        try {
            newConverter(null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("names");
        }

        try {
            newConverter(new ArrayList<String>());
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("names");
        }
    }

    public void apply_should_reject_values_with_different_size_than_names() {
        List<String> names = newArrayList(N1);
        List<Object> values = newArrayList(V1, V2);
        try {
            newConverter(names).apply(values);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("Names and values must have the same size but");
            assertThat(e).hasMessageContaining(names.toString());
            assertThat(e).hasMessageContaining(values.toString());
        }
    }

    @SuppressWarnings("unchecked")
    public void apply_should_convert_values_with_same_size_than_names() {
        List<String> names = newArrayList(N1, N2);
        List<Object> values = newArrayList(V1, V2);
        DBObject document = newConverter(names).apply(values);
        assertThat(document.toMap()).containsOnly(entry(N1, V1), entry(N2, V2));
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static MongoStateIdConverter newConverter(List<String> names) {
        return new MongoStateIdConverter(names);
    }
}
