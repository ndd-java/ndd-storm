package name.didier.david.storm.mongodb;

import static java.lang.String.format;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.storm.mongodb.MongoStateSettings.DEFAULT_BULK_SIZE;
import static name.didier.david.storm.mongodb.MongoStateSettings.DEFAULT_CACHE_SIZE;
import static name.didier.david.test4j.testng.TestNgDataProviders.BLANK_STRINGS;
import static name.didier.david.test4j.testng.TestNgDataProviders.EMPTY_LISTS;
import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import name.didier.david.test4j.testng.TestNgDataProviders;

@Test(groups = UNIT)
public class MongoStateSettingsTest {

    private static final String URI = "some/uri";
    private static final List<String> KEY_FIELDS = newArrayList("k1", "k2");
    private static final List<String> VALUE_FIELDS = newArrayList("v1", "v2", "v3");

    private MongoStateSettings settings;

    @BeforeMethod
    protected void create_configuration() {
        settings = settings(URI, KEY_FIELDS, VALUE_FIELDS);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = BLANK_STRINGS)
    public void constructor_should_reject_blank_uri(String uri) {
        try {
            settings(uri, KEY_FIELDS, VALUE_FIELDS);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("uri");
        }
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = EMPTY_LISTS)
    public void constructor_should_reject_empty_keys(List<String> keyFields) {
        try {
            settings(URI, keyFields, VALUE_FIELDS);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("keyFields");
        }
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = EMPTY_LISTS)
    public void constructor_should_reject_empty_values(List<String> valueFields) {
        try {
            settings(URI, KEY_FIELDS, valueFields);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("valueFields");
        }
    }

    public void test_uri() {
        assertThat(settings.getUri()).isEqualTo(URI);
    }

    public void test_key_fields() {
        assertThat(settings.getKeyFields()).isEqualTo(KEY_FIELDS);
    }

    public void test_value_fields() {
        assertThat(settings.getValueFields()).isEqualTo(VALUE_FIELDS);
    }

    public void test_bulk_size() {
        assertThat(settings.getBulkSize()).isEqualTo(DEFAULT_BULK_SIZE);

        final int newBulkSize = 10;
        settings.setBulkSize(newBulkSize);
        assertThat(settings.getBulkSize()).isEqualTo(newBulkSize);
    }

    public void test_cache_size() {
        assertThat(settings.getCacheSize()).isEqualTo(DEFAULT_CACHE_SIZE);

        final int newCacheSize = 20;
        settings.setCacheSize(newCacheSize);
        assertThat(settings.getCacheSize()).isEqualTo(newCacheSize);
    }

    public void test_toString() {
        assertThat(settings.toString()).contains("uri=some/uri");
        assertThat(settings.toString()).contains(format("keyFields=%s", KEY_FIELDS));
        assertThat(settings.toString()).contains(format("valueFields=%s", VALUE_FIELDS));
        assertThat(settings.toString()).contains(format("bulkSize=%s", DEFAULT_BULK_SIZE));
        assertThat(settings.toString()).contains(format("cacheSize=%s", DEFAULT_CACHE_SIZE));
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static MongoStateSettings settings(String uri, List<String> keyFields, List<String> valueFields) {
        return new MongoStateSettings(uri, keyFields, valueFields);
    }
}
