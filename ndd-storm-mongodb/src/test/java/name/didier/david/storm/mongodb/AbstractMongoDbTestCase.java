package name.didier.david.storm.mongodb;

import static java.lang.String.format;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.util.UUID;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mongodb.DB;
import com.mongodb.MongoClient;

import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.mongo.tests.MongodForTestsFactory;

@Test(groups = UNIT)
public abstract class AbstractMongoDbTestCase {

    private static final String MONGO_URI_FORMAT = "mongodb://localhost:%d/%s.%s";

    private static MongodForTestsFactory mongoFactory;
    private static MongoClient mongoClient;
    private static String mongoDbName;

    @BeforeClass
    protected static void start_mongo()
            throws Exception {
        mongoFactory = MongodForTestsFactory.with(Version.Main.PRODUCTION);
        mongoClient = mongoFactory.newMongo();
        mongoDbName = "MongoDbTestCase-" + UUID.randomUUID();
    }

    @AfterClass
    protected static void stop_mongo() {
        if (mongoFactory != null) {
            mongoFactory.shutdown();
        }
    }

    protected static MongoClient mongoClient() {
        return mongoClient;
    }

    protected static DB mongoDb() {
        return mongoClient.getDB(mongoDbName);
    }

    protected static String mongoHost() {
        return mongoClient.getAddress().getHost();
    }

    protected static int mongoPort() {
        return mongoClient.getAddress().getPort();
    }

    protected static String mongoUri(String collectionName) {
        return format(MONGO_URI_FORMAT, mongoPort(), mongoDbName, collectionName);
    }
}
