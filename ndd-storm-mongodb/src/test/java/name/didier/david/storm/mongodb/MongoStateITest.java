package name.didier.david.storm.mongodb;

import static java.util.concurrent.TimeUnit.SECONDS;

import static org.hamcrest.Matchers.equalTo;

import static com.google.common.collect.ArrayListMultimap.create;
import static com.google.common.collect.Lists.newArrayList;
import static com.jayway.awaitility.Awaitility.waitAtMost;

import static clojure.lang.Numbers.add;
import static name.didier.david.test4j.testng.TestNgGroups.INTEGRATION;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.common.collect.Multimap;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import name.didier.david.storm.test.spout.CyclingBatchSpout;
import name.didier.david.storm.test.utils.EclipseUtils;
import storm.trident.TridentTopology;
import storm.trident.operation.CombinerAggregator;
import storm.trident.operation.builtin.Sum;
import storm.trident.state.StateFactory;
import storm.trident.tuple.TridentTuple;

@Test(groups = INTEGRATION)
public class MongoStateITest
        extends AbstractMongoDbTestCase {
    // CSOFF: ClassFanOutComplexityCheck -3
    // CSOFF: ClassDataAbstractionCouplingCheck -4

    /** The associated logger. */
    private static final Logger logger = LoggerFactory.getLogger(MongoStateITest.class);

    /** The number of field in a tuple . */
    private static final int FIELD_COUNT = 4;
    /** The number of tuples in a batch. */
    private static final int BATCH_SIZE = 10;
    /** The number of batches. */
    private static final int BATCH_COUNT = 3;
    /** Data of a single batch. */
    private static final long[][] DATA = new long[BATCH_SIZE][FIELD_COUNT];

    private static final int TIMEOUT = 10;

    @BeforeClass
    private static void create_data_set() {
        // ------------> FIELD_COUNT
        // | 0, 0, 0, 0
        // | 1, 1, 1, 1
        // | 2, 0, 2, 0
        // | 3, 1, 3, 1
        // | ...
        // | 9, 1, 9, 1
        // v
        // BATCH_SIZE
        for (int i = 0; i < DATA.length; i++) {
            DATA[i] = new long[] { i, i % 2, i, i % 2 };
            logger.debug("DATA[{}] = {}", i, DATA[i]);
        }
    }

    @Test(dataProvider = "one_key_and_one_value", enabled = true)
    public void test_states_with_one_key_and_one_value(String type, StateFactory stateFactory)
            throws Exception {
        Multimap<List<Long>, Long> expectedDataMap = create();
        for (long i = 0; i < BATCH_SIZE; i++) {
            expectedDataMap.putAll(newArrayList(i), newArrayList(i * BATCH_COUNT));
        }

        DB db = mongoDb();
        DBCollection collection = db.createCollection("c_11_" + type, new BasicDBObject());

        // @formatter:off
        CyclingBatchSpout cbs = new CyclingBatchSpout(
                new Fields("group_by_A", "value_1"),
                outputs(0, 2),
                BATCH_SIZE, BATCH_COUNT);
        TridentTopology topology = new TridentTopology();
        topology.newStream("stream_11_" + type, cbs)
                .groupBy(new Fields("group_by_A"))
                .persistentAggregate(
                        stateFactory,
                        new Fields("value_1"),
                        new Sum(),
                        new Fields("summary_11"));
        // @formatter:on

        LocalCluster cluster = EclipseUtils.newLocalCluster();
        cluster.submitTopology("t_11_" + type, new Config(), topology.build());

        waitAtMost(TIMEOUT, SECONDS).until(sizeOf(collection), equalTo((long) BATCH_SIZE));
        waitAtMost(TIMEOUT, SECONDS).until(dataOf11(collection), equalTo(expectedDataMap));
        logger.debug("t_11_{} first element: {}", type, collection.find().toArray().get(0));

        cluster.shutdown();
    }

    @Test(dataProvider = "one_key_and_two_values", enabled = true)
    public void test_states_with_one_key_and_two_values(String type, StateFactory stateFactory)
            throws Exception {
        Multimap<List<Long>, Long> expectedDataMap = create();
        for (long i = 0; i < BATCH_SIZE; i++) {
            expectedDataMap.putAll(newArrayList(i), newArrayList(i * BATCH_COUNT, (i % 2) * BATCH_COUNT));
        }

        DB db = mongoDb();
        DBCollection collection = db.createCollection("c_12_" + type, new BasicDBObject());

        // @formatter:off
        CyclingBatchSpout cbs = new CyclingBatchSpout(
                new Fields("group_by_A", "value_1", "value_2"),
                outputs(0, 2, 3),
                BATCH_SIZE, BATCH_COUNT);
        TridentTopology topology = new TridentTopology();
        topology.newStream("stream_12_" + type, cbs)
                .groupBy(new Fields("group_by_A"))
                .persistentAggregate(
                        stateFactory,
                        new Fields("value_1", "value_2"),
                        new SumSum(),
                        new Fields("summary_12"));
        // @formatter:on

        LocalCluster cluster = EclipseUtils.newLocalCluster();
        cluster.submitTopology("t_12_" + type, new Config(), topology.build());

        waitAtMost(TIMEOUT, SECONDS).until(sizeOf(collection), equalTo((long) BATCH_SIZE));
        waitAtMost(TIMEOUT, SECONDS).until(dataOf12(collection), equalTo(expectedDataMap));
        logger.debug("t_12_{} first element: {}", type, collection.find().toArray().get(0));

        cluster.shutdown();
    }

    @Test(dataProvider = "two_keys_and_one_value", enabled = true)
    public void test_states_with_two_keys_and_one_value(String type, StateFactory stateFactory)
            throws Exception {
        Multimap<List<Long>, Long> expectedDataMap = create();
        for (long i = 0; i < BATCH_SIZE; i++) {
            expectedDataMap.putAll(newArrayList(i, i % 2), newArrayList(i * BATCH_COUNT));
        }

        DB db = mongoDb();
        DBCollection collection = db.createCollection("c_21_" + type, new BasicDBObject());

        // @formatter:off
        CyclingBatchSpout cbs = new CyclingBatchSpout(
                new Fields("group_by_A", "group_by_B", "value_1"),
                outputs(0, 1, 2),
                BATCH_SIZE, BATCH_COUNT);
        TridentTopology topology = new TridentTopology();
        topology.newStream("stream_21_" + type, cbs)
                .groupBy(new Fields("group_by_A", "group_by_B"))
                .persistentAggregate(
                        stateFactory,
                        new Fields("value_1"),
                        new Sum(),
                        new Fields("summary_21"));
        // @formatter:on

        LocalCluster cluster = EclipseUtils.newLocalCluster();
        cluster.submitTopology("t_21_" + type, new Config(), topology.build());

        waitAtMost(TIMEOUT, SECONDS).until(sizeOf(collection), equalTo((long) BATCH_SIZE));
        waitAtMost(TIMEOUT, SECONDS).until(dataOf21(collection), equalTo(expectedDataMap));
        logger.debug("t_21_{} first element: {}", type, collection.find().toArray().get(0));

        cluster.shutdown();
    }

    @Test(dataProvider = "two_keys_and_two_values", enabled = true)
    public void test_states_with_two_keys_and_two_values(String type, StateFactory stateFactory)
            throws Exception {
        Multimap<List<Long>, Long> expectedDataMap = create();
        for (long i = 0; i < BATCH_SIZE; i++) {
            expectedDataMap.putAll(newArrayList(i, i % 2), newArrayList(i * BATCH_COUNT, (i % 2) * BATCH_COUNT));
        }

        DB db = mongoDb();
        DBCollection collection = db.createCollection("c_22_" + type, new BasicDBObject());

        // @formatter:off
        CyclingBatchSpout cbs = new CyclingBatchSpout(
                new Fields("group_by_A", "group_by_B", "value_1", "value_2"),
                outputs(0, 1, 2, 3),
                BATCH_SIZE, BATCH_COUNT);
        TridentTopology topology = new TridentTopology();
        topology.newStream("stream_22_" + type, cbs)
                .groupBy(new Fields("group_by_A", "group_by_B"))
                .persistentAggregate(
                        stateFactory,
                        new Fields("value_1", "value_2"),
                        new SumSum(),
                        new Fields("summary_22"));
        // @formatter:on

        LocalCluster cluster = EclipseUtils.newLocalCluster();
        cluster.submitTopology("t_22_" + type, new Config(), topology.build());

        waitAtMost(TIMEOUT, SECONDS).until(sizeOf(collection), equalTo((long) BATCH_SIZE));
        waitAtMost(TIMEOUT, SECONDS).until(dataOf22(collection), equalTo(expectedDataMap));
        logger.debug("t_22_{} first element: {}", type, collection.find().toArray().get(0));

        cluster.shutdown();
    }

    // -----------------------------------------------------------------------------------------------------------------

    public List<Values> outputs(int... indexes) {
        List<Values> outputs = new ArrayList<>(BATCH_SIZE);
        for (int i = 0; i < BATCH_SIZE; i++) {
            Values values = new Values();
            for (int index : indexes) {
                values.add(DATA[i][index]);
            }
            outputs.add(values);
        }
        return outputs;
    }

    @DataProvider(name = "one_key_and_one_value")
    private Object[][] one_key_and_one_value() {
        String[] keyFields = new String[] { "group_by_A" };
        String[] valueFields = new String[] { "sum_1" };

        // @formatter:off
        return new Object[][] {
            { "non_transactional", MongoStateFactory.nonTransactional(
                    new MongoStateSettings(mongoUri("c_11_non_transactional"), keyFields, valueFields)), },
            { "transactional", MongoStateFactory.transactional(
                    new MongoStateSettings(mongoUri("c_11_transactional"), keyFields, valueFields)), },
            { "opaque", MongoStateFactory.opaque(
                    new MongoStateSettings(mongoUri("c_11_opaque"), keyFields, valueFields)), },
        };
        // @formatter:on
    }

    @DataProvider(name = "one_key_and_two_values")
    private Object[][] one_key_and_two_values() {
        String[] keyFields = new String[] { "group_by_A" };
        String[] valueFields = new String[] { "sum_1", "sum_2" };

        // @formatter:off
        return new Object[][] {
            { "non_transactional", MongoStateFactory.nonTransactional(
                    new MongoStateSettings(mongoUri("c_12_non_transactional"), keyFields, valueFields)), },
            { "transactional", MongoStateFactory.transactional(
                    new MongoStateSettings(mongoUri("c_12_transactional"), keyFields, valueFields)), },
            { "opaque", MongoStateFactory.opaque(
                    new MongoStateSettings(mongoUri("c_12_opaque"), keyFields, valueFields)), },
        };
        // @formatter:on
    }

    @DataProvider(name = "two_keys_and_one_value")
    private Object[][] two_keys_and_one_value() {
        String[] keyFields = new String[] { "group_by_A", "group_by_B" };
        String[] valueFields = new String[] { "sum_1" };

        // @formatter:off
        return new Object[][] {
            { "non_transactional", MongoStateFactory.nonTransactional(
                    new MongoStateSettings(mongoUri("c_21_non_transactional"), keyFields, valueFields)), },
            { "transactional", MongoStateFactory.transactional(
                    new MongoStateSettings(mongoUri("c_21_transactional"), keyFields, valueFields)), },
            { "opaque", MongoStateFactory.opaque(
                    new MongoStateSettings(mongoUri("c_21_opaque"), keyFields, valueFields)), },
        };
        // @formatter:on
    }

    @DataProvider(name = "two_keys_and_two_values")
    private Object[][] two_keys_and_two_values() {
        String[] keyFields = new String[] { "group_by_A", "group_by_B" };
        String[] valueFields = new String[] { "sum_1", "sum_2" };

        // @formatter:off
        return new Object[][] {
            { "non_transactional", MongoStateFactory.nonTransactional(
                    new MongoStateSettings(mongoUri("c_22_non_transactional"), keyFields, valueFields)), },
            { "transactional", MongoStateFactory.transactional(
                    new MongoStateSettings(mongoUri("c_22_transactional"), keyFields, valueFields)), },
            { "opaque", MongoStateFactory.opaque(
                    new MongoStateSettings(mongoUri("c_22_opaque"), keyFields, valueFields)), },
        };
        // @formatter:on
    }

    private static Callable<Long> sizeOf(final DBCollection collection) {
        return new Callable<Long>() {
            @Override
            public Long call()
                    throws Exception {
                return collection.count();
            }
        };
    }

    private static Callable<Multimap<List<Long>, Long>> dataOf11(final DBCollection collection) {
        return new DataOf11(collection);
    }

    private static Callable<Multimap<List<Long>, Long>> dataOf12(final DBCollection collection) {
        return new DataOf12(collection);
    }

    private static Callable<Multimap<List<Long>, Long>> dataOf21(final DBCollection collection) {
        return new DataOf21(collection);
    }

    private static Callable<Multimap<List<Long>, Long>> dataOf22(final DBCollection collection) {
        return new DataOf22(collection);
    }

    // -----------------------------------------------------------------------------------------------------------------

    private abstract static class AbstractDataOf
            implements Callable<Multimap<List<Long>, Long>> {
        protected final DBCollection collection;

        protected AbstractDataOf(DBCollection collection) {
            this.collection = collection;
        }

        @Override
        public Multimap<List<Long>, Long> call()
                throws Exception {
            List<DBObject> actualDataList = collection.find().toArray();
            return toMap(actualDataList);
        }

        private Multimap<List<Long>, Long> toMap(List<DBObject> documents) {
            Multimap<List<Long>, Long> dataMap = create();

            for (DBObject document : documents) {
                DBObject dbKey = (DBObject) document.get("_id");
                DBObject dbValue = (DBObject) document.get("value");
                dataMap.putAll(extractKeys(dbKey), extractValues(dbValue));
            }

            return dataMap;
        }

        protected abstract List<Long> extractKeys(DBObject dbKey);

        protected abstract List<Long> extractValues(DBObject dbValue);
    }

    private static final class DataOf11
            extends AbstractDataOf {

        public DataOf11(DBCollection collection) {
            super(collection);
        }

        @Override
        protected List<Long> extractKeys(DBObject dbKey) {
            return newArrayList((Long) dbKey.get("group_by_A"));
        }

        @Override
        protected List<Long> extractValues(DBObject dbValue) {
            return newArrayList((Long) dbValue.get("sum_1"));
        }
    }

    private static final class DataOf12
            extends AbstractDataOf {

        public DataOf12(DBCollection collection) {
            super(collection);
        }

        @Override
        protected List<Long> extractKeys(DBObject dbKey) {
            return newArrayList((Long) dbKey.get("group_by_A"));
        }

        @Override
        protected List<Long> extractValues(DBObject dbValue) {
            return newArrayList((Long) dbValue.get("sum_1"), (Long) dbValue.get("sum_2"));
        }
    }

    private static final class DataOf21
            extends AbstractDataOf {

        public DataOf21(DBCollection collection) {
            super(collection);
        }

        @Override
        protected List<Long> extractKeys(DBObject dbKey) {
            return newArrayList((Long) dbKey.get("group_by_A"), (Long) dbKey.get("group_by_B"));
        }

        @Override
        protected List<Long> extractValues(DBObject dbValue) {
            return newArrayList((Long) dbValue.get("sum_1"));
        }
    }

    private static final class DataOf22
            extends AbstractDataOf {

        public DataOf22(DBCollection collection) {
            super(collection);
        }

        @Override
        protected List<Long> extractKeys(DBObject dbKey) {
            return newArrayList((Long) dbKey.get("group_by_A"), (Long) dbKey.get("group_by_B"));
        }

        @Override
        protected List<Long> extractValues(DBObject dbValue) {
            return newArrayList((Long) dbValue.get("sum_1"), (Long) dbValue.get("sum_2"));
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class SumSum
            implements CombinerAggregator<List<Number>> {
        private static final long serialVersionUID = 1L;

        public SumSum() {
            super();
        }

        @Override
        public List<Number> init(TridentTuple tuple) {
            return newArrayList((Number) tuple.getValue(0), (Number) tuple.getValue(1));
        }

        @Override
        public List<Number> combine(List<Number> val1, List<Number> val2) {
            // @formatter:off
            return newArrayList(
                    add(val1.get(0), val2.get(0)),
                    add(val1.get(1), val2.get(1)));
            // @formatter:on
        }

        @Override
        public List<Number> zero() {
            return newArrayList((Number) 0, (Number) 0);
        }

    }
}
