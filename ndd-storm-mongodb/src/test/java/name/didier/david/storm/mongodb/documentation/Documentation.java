package name.didier.david.storm.mongodb.documentation;

import static com.google.common.collect.Lists.newArrayList;

import static clojure.lang.Numbers.add;

import java.util.List;

import backtype.storm.topology.IRichSpout;
import backtype.storm.tuple.Fields;
import name.didier.david.storm.mongodb.MongoStateFactory;
import name.didier.david.storm.mongodb.MongoStateSettings;
import storm.trident.TridentTopology;
import storm.trident.operation.CombinerAggregator;
import storm.trident.state.StateFactory;
import storm.trident.tuple.TridentTuple;

public class Documentation {

    public void documentation() {
        // @formatter:off
        // output fields: group_by_A, group_by_B, value_1, value_2
        IRichSpout spout = null;

        StateFactory stateFactory =
            MongoStateFactory.nonTransactional(
            // or MongoStateFactory.transactional(
            // or MongoStateFactory.opaque(
                new MongoStateSettings(
                    "mongodb://<host>:<port>/<db>.<collection>",
                    new String[] { "group_by_A", "group_by_B" },
                    new String[] { "sum_1", "sum_2" }));

        TridentTopology topology = new TridentTopology();
        topology.newStream("stream_non_transactional", spout)
                .groupBy(new Fields("group_by_A", "group_by_B"))
                .persistentAggregate(
                        stateFactory,
                        new Fields("value_1", "value_2"),
                        new SumSum(),
                        new Fields("sumsum"));
        // @formatter:on
    }

    private static class SumSum
            implements CombinerAggregator<List<Number>> {
        private static final long serialVersionUID = 1L;

        public SumSum() {
            super();
        }

        @Override
        public List<Number> init(TridentTuple tuple) {
            return newArrayList((Number) tuple.getValue(0), (Number) tuple.getValue(1));
        }

        @Override
        public List<Number> combine(List<Number> val1, List<Number> val2) {
            // @formatter:off
            return newArrayList(
                    add(val1.get(0), val2.get(0)),
                    add(val1.get(1), val2.get(1)));
            // @formatter:on
        }

        @Override
        public List<Number> zero() {
            return newArrayList((Number) 0, (Number) 0);
        }

    }
}
