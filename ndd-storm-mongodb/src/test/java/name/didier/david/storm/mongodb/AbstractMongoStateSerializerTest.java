package name.didier.david.storm.mongodb;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.when;

import static com.google.common.collect.ImmutableMap.of;
import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.util.HashSet;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import name.didier.david.test4j.testng.TestNgMockitoListener;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public class AbstractMongoStateSerializerTest
        extends AbstractMongoStateSerializerTestCase {

    public void documentToValue_should_process_a_null_document() {
        List<String> valueFields = newArrayList(K1);
        assertThat(documentToValue(null, valueFields)).isNull();
    }

    public void documentToValue_should_process_a_single_value() {
        List<String> valueFields = newArrayList(K1);
        DBObject document = new BasicDBObject(K1, V1);
        assertThat(documentToValue(document, valueFields)).isEqualTo(V1);
    }

    public void documentToValue_should_process_a_single_null_value() {
        List<String> valueFields = newArrayList(K1);
        DBObject document = new BasicDBObject(K1, null);
        assertThat(documentToValue(document, valueFields)).isEqualTo(null);
    }

    public void documentToValue_should_process_multiple_values() {
        List<String> valueFields = newArrayList(K1, K2);
        DBObject document = new BasicDBObject(of(K1, V1, K2, V2));
        assertThat(documentToValue(document, valueFields)).isEqualTo(newArrayList(V1, V2));
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void valueToDocument_should_process_a_single_value() {
        Object value = V1;
        List<String> valueFields = newArrayList(K1);
        assertThat(valueToDocument(value, valueFields)).containsOnly(entry(K1, V1));
    }

    public void valueToDocument_should_process_a_single_null_value() {
        Object value = null;
        List<String> valueFields = newArrayList(K1);
        assertThat(valueToDocument(value, valueFields)).containsOnly(entry(K1, null));
    }

    public void valueToDocument_should_process_multiple_values() {
        Object value = newArrayList(V1, V2);
        List<String> valueFields = newArrayList(K1, K2);
        assertThat(valueToDocument(value, valueFields)).containsOnly(entry(K1, V1), entry(K2, V2));
    }

    public void valueToDocument_should_process_multiple_null_values() {
        Object value = null;
        List<String> valueFields = newArrayList(K1, K2);
        assertThat(valueToDocument(value, valueFields)).containsOnly(entry(K1, null), entry(K2, null));
    }

    public void valueToDocument_should_reject_multiple_values_without_matching_keys() {
        Object value = newArrayList(V1);
        List<String> valueFields = newArrayList(K1, K2);
        try {
            valueToDocument(value, valueFields);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("Names and values must have the same size but");
            assertThat(e).hasMessageContaining(value.toString());
            assertThat(e).hasMessageContaining(valueFields.toString());
        }
    }

    public void valueToDocument_should_reject_() {
        Object value = new HashSet<>();
        List<String> valueFields = newArrayList(K1, K2);
        when(settings.getValueFields()).thenReturn(valueFields);
        try {
            newSerializer(settings).valueToDocument(value);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("Expected value to be");
            assertThat(e).hasMessageContaining(List.class.getName());
            assertThat(e).hasMessageContaining(HashSet.class.getName());
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Override
    protected AbstractMongoStateSerializer<Object> newSerializer(MongoStateSettings stateSettings) {
        // CSOFF: IllegalTypeCheck -1
        return new MongoStateSerializerImpl(stateSettings);
    }

    private Map<?, ?> valueToDocument(Object value, List<String> valueFields) {
        when(settings.getValueFields()).thenReturn(valueFields);
        return newSerializer(settings).valueToDocument(value).toMap();
    }

    private Object documentToValue(DBObject document, List<String> valueFields) {
        when(settings.getValueFields()).thenReturn(valueFields);
        return newSerializer(settings).documentToValue(document);
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static class MongoStateSerializerImpl
            extends AbstractMongoStateSerializer<Object> {
        private static final long serialVersionUID = 1L;

        public MongoStateSerializerImpl(MongoStateSettings settings) {
            super(settings);
        }

        @Override
        public DBObject serialize(DBObject id, Object value) {
            return null;
        }

        @Override
        public Object deserialize(DBObject document) {
            return null;
        }
    }
}
