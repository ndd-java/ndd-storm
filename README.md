# NDD Storm

The parent for all NDD Storm projects which aggregates common settings.

## About

Thanks to the [Apache Storm community](https://storm.incubator.apache.org/community.html) !

Copyright David DIDIER 2014
