# NDD Storm Test

NDD Storm Test provides testing support for [Apache Storm](https://storm.incubator.apache.org/). Released under the [LGPL](https://www.gnu.org/licenses/lgpl.html).

## Usage

### Assertions

```java
import static name.didier.david.storm.test.assertions.StormAssertions.assertThat;
```

Assert `backtype.storm.topology.IComponent`:

```java
assertThat(iComponent).declareExactly("field_1", "field_2");
```

Assert `backtype.storm.task.OutputCollector`:

```java
OutputCollector outputCollector = mock(OutputCollector.class);
// use this collector in some component then:

// tuple acknowledgement
assertThat(outputCollector).acked(expectedTuple1, expectedTuple2, expectedTuple3);
assertThat(outputCollector).ackedExactly(expectedTuple1, expectedTuple2, expectedTuple3);
assertThat(outputCollector).ackedOnly(expectedTuple1, expectedTuple2, expectedTuple3);

assertThat(outputCollector).didNotAck(expectedTuple1, expectedTuple2, expectedTuple3);

// values emission
assertThat(outputCollector).emitted(expectedValues1, expectedValues2, expectedValues3);
assertThat(outputCollector).emittedExactly(expectedValues1, expectedValues2, expectedValues3);
assertThat(outputCollector).emittedOnly(expectedValues1, expectedValues2, expectedValues3);

assertThat(outputCollector).didNotEmit(expectedValues1, expectedValues2, expectedValues3);

// tuple failure
assertThat(outputCollector).failed(expectedTuple1, expectedTuple2, expectedTuple3);
assertThat(outputCollector).failedExactly(expectedTuple1, expectedTuple2, expectedTuple3);
assertThat(outputCollector).failedOnly(expectedTuple1, expectedTuple2, expectedTuple3);

assertThat(outputCollector).didNotFail(expectedTuple1, expectedTuple2, expectedTuple3);
```

Assert `storm.trident.operation.TridentCollector`:

```java
TridentCollector tridentCollector = mock(TridentCollector.class);
// use this collector in some component then:

// values emission
assertThat(tridentCollector).emitted(expectedValues1, expectedValues2, expectedValues3);
assertThat(tridentCollector).emittedExactly(expectedValues1, expectedValues2, expectedValues3);
assertThat(tridentCollector).emittedOnly(expectedValues1, expectedValues2, expectedValues3);

assertThat(tridentCollector).didNotEmit(expectedValues1, expectedValues2, expectedValues3);
```

## Setup

Using Maven:

```xml
<dependency>
  <groupId>name.didier.david.storm</groupId>
  <artifactId>ndd-storm-test</artifactId>
  <version>X.Y.Z</version>
  <scope>test</scope>
</dependency>
```

## Reports

Maven reports are available at [https://ndd-java.bitbucket.org/ndd-storm-test/project-reports.html](https://ndd-java.bitbucket.org/ndd-storm-test/project-reports.html)

## About

Thanks to the [Apache Storm community](https://storm.incubator.apache.org/community.html) !

Copyright David DIDIER 2014
