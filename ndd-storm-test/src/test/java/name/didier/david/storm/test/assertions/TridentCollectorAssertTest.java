package name.didier.david.storm.test.assertions;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import backtype.storm.tuple.Values;
import name.didier.david.test4j.testng.TestNgMockitoListener;
import storm.trident.operation.TridentCollector;
import storm.trident.operation.impl.CaptureCollector;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public class TridentCollectorAssertTest
        extends AbstractCollectorAssertTestCase {

    @Mock
    private TridentCollector collector;

    private TridentCollectorAssert asserter;
    private TridentCollectorAssert asserterOnActual;

    @BeforeMethod
    public void create_asserter() {
        asserter = new TridentCollectorAssert(collector);
        asserterOnActual = new TridentCollectorAssert(new CaptureCollector());
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedValues")
    public void emitted_should_pass_when_values_are_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emitted(expectedValues);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void emitted_should_fail_when_the_collector_is_not_a_mock() {
        collector.emit(new Values());
        asserterOnActual.emitted(new Values());
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedValues")
    public void emitted_should_fail_when_any_value_is_not_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emitted(expectedValues);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedOnlyValues")
    public void emittedOnly_should_pass_when_values_are_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emittedOnly(expectedValues);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void emittedOnly_should_fail_when_the_collector_is_not_a_mock() {
        collector.emit(new Values());
        asserterOnActual.emittedOnly(new Values());
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedOnlyValues")
    public void emittedOnly_should_fail_when_any_value_is_not_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emittedOnly(expectedValues);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedExactlyValues")
    public void emittedExactly_should_pass_when_values_are_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emittedExactly(expectedValues);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void emittedExactly_should_fail_when_the_collector_is_not_a_mock() {
        collector.emit(new Values());
        asserterOnActual.emittedExactly(new Values());
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedExactlyValues")
    public void emittedExactly_should_fail_when_any_value_is_not_emitted(Values[] actualValues,
            Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emittedExactly(expectedValues);
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void didNotEmit_should_pass_when_values_are_not_emitted() {
        collector.emit(new Values(1));
        asserter.didNotEmit(new Values(2));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void didNotEmit_should_fail_when_the_collector_is_not_a_mock() {
        collector.emit(new Values());
        asserterOnActual.didNotEmit(new Values());
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyNotContainedValues")
    public void didNotEmit_should_fail_when_any_value_is_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.didNotEmit(expectedValues);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** Contains, in any order. */
    @DataProvider(name = "unsuccessfullyNotContainedValues")
    private Object[][] unsuccessfullyNotContainedValues() {
        // @formatter:off
        return new Values[][][] {
            { { v(1)       }, { v(1)       } },
            { { v(1)       }, { v(1), v(2) } },
            { { v(1), v(2) }, { v(1)       } },
            { { v(1), v(2) }, { v(1), v(2) } },
            { { v(1), v(2) }, { v(2), v(1) } },
        };
        // @formatter:on
    }
}
