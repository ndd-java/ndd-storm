package name.didier.david.storm.test.spout;

import static com.google.common.collect.Lists.newArrayList;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.util.List;

import org.mockito.Mock;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import name.didier.david.test4j.testng.TestNgMockitoListener;
import storm.trident.operation.TridentCollector;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public abstract class AbstractBatchSpoutTestCase {

    protected static final Fields FIELDS = new Fields("field_1", "field_2");
    protected static final List<Values> DATA = newArrayList(values(1, 2, 3));
    protected static final int DATA_SIZE = DATA.size();
    protected static final int BATCH_SIZE = 3;

    @Mock
    protected TridentCollector collector;

    protected AbstractBatchSpoutTestCase() {
        super();
    }

    protected static Values values(int i) {
        return new Values("value_" + i + "_1", "value_" + i + "_2");
    }

    protected static Values[] values(int... indexes) {
        Values[] array = new Values[indexes.length];
        for (int i = 0; i < array.length; i++) {
            array[i] = values(indexes[i]);
        }
        return array;
    }
}
