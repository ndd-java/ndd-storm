package name.didier.david.storm.test.spout;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static name.didier.david.storm.test.assertions.StormAssertions.assertThat;
import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.mockito.Mock;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import backtype.storm.spout.SpoutOutputCollector;
import name.didier.david.test4j.testng.TestNgMockitoListener;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public class NoOpSpoutTest {

    private static final String[] FIELDS = new String[] { "field_1", "field_2" };

    @Mock
    private SpoutOutputCollector collector;

    // -----------------------------------------------------------------------------------------------------------------

    public void constructor_should_reject_null_fields() {
        try {
            spout((String[]) null);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("outputFields");
        }
    }

    public void constructor_should_reject_empty_fields() {
        try {
            spout(new String[0]);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("outputFields");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void open_should_do_nothing() {
        openedSpout(FIELDS);
        // TODO
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void nextTuple_should_emit_nothing() {
        NoOpSpout spout = openedSpout(FIELDS);
        spout.nextTuple();
        // TODO create SpoutOutputCollectorAssert
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void outputFields_should_be_readable() {
        NoOpSpout spout = openedSpout(FIELDS);
        assertThat(spout).declareExactly(FIELDS);
    }

    // -----------------------------------------------------------------------------------------------------------------

    private NoOpSpout spout(String... fields) {
        return new NoOpSpout(fields);
    }

    private NoOpSpout openedSpout(String... fields) {
        NoOpSpout spout = spout(fields);
        spout.open(null, null, collector);
        return spout;
    }
}
