package name.didier.david.storm.test.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.mockito.internal.util.MockUtil;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import backtype.storm.tuple.Tuple;

@Test(groups = UNIT)
public class TupleMapTest {

    private TupleMap map;

    @BeforeMethod
    public void create_map() {
        map = new TupleMap();
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void tuple_should_return_a_mock() {
        Tuple tuple = map.tuple(1);
        if (!new MockUtil().isMock(tuple)) {
            fail("Tuple must be a mock but is " + tuple.getClass());
        }
    }

    public void tuple_should_return_a_different_tuple_for_different_key() {
        assertThat(map.tuple(1)).isNotEqualTo(map.tuple(2));
        assertThat(map.tuple(1)).isNotEqualTo(map.tuple(3));
        assertThat(map.tuple(2)).isNotEqualTo(map.tuple(3));
    }

    public void tuple_should_return_the_same_tuple_for_the_same_key() {
        assertThat(map.tuple(1)).isSameAs(map.tuple(1));
        assertThat(map.tuple(2)).isSameAs(map.tuple(2));
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void t_should_return_a_mock() {
        Tuple tuple = map.t(1);
        if (!new MockUtil().isMock(tuple)) {
            fail("Tuple must be a mock but is " + tuple.getClass());
        }
    }

    public void t_should_return_a_different_tuple_for_different_key() {
        assertThat(map.t(1)).isNotEqualTo(map.t(2));
        assertThat(map.t(1)).isNotEqualTo(map.t(3));
        assertThat(map.t(2)).isNotEqualTo(map.t(3));
    }

    public void t_should_return_the_same_tuple_for_the_same_key() {
        assertThat(map.t(1)).isSameAs(map.t(1));
        assertThat(map.t(2)).isSameAs(map.t(2));
    }
}
