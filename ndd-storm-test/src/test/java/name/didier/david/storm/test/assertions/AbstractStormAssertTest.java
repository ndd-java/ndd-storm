package name.didier.david.storm.test.assertions;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;
import static org.mockito.Mockito.mock;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.Test;

@Test(groups = UNIT)
public class AbstractStormAssertTest {

    public void checkIsMock_should_pass_when_given_a_mock() {
        StormAssertImpl asserter = newStormAssert(mock(Object.class));
        assertThat(asserter.isMock()).isSameAs(asserter);
    }

    public void checkIsMock_should_fail_when_given_not_a_mock() {
        StormAssertImpl asserter = newStormAssert(new Object());
        try {
            asserter.isMock();
            failBecauseExceptionWasNotThrown(AssertionError.class);
        } catch (AssertionError e) {
            assertThat(e).hasMessage("Expecting class java.lang.Object to be a mock");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    private StormAssertImpl newStormAssert(Object actual) {
        return new StormAssertImpl(actual);
    }

    private static class StormAssertImpl
            extends AbstractStormAssert<StormAssertImpl, Object> {

        public StormAssertImpl(Object actual) {
            super(actual, Object.class);
        }
    }
}
