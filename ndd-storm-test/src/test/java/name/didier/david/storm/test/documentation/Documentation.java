package name.didier.david.storm.test.documentation;

import static org.mockito.Mockito.mock;

import static name.didier.david.storm.test.assertions.StormAssertions.assertThat;

import backtype.storm.task.OutputCollector;
import backtype.storm.topology.IComponent;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import storm.trident.operation.TridentCollector;

public class Documentation {

    public void document_IComponentAssert() {
        IComponent iComponent = mock(IComponent.class);
        assertThat(iComponent).declareExactly("field_1", "field_2");
    }

    public void document_OutputCollectorAssert() {
        Tuple expectedTuple1 = mock(Tuple.class);
        Tuple expectedTuple2 = mock(Tuple.class);
        Tuple expectedTuple3 = mock(Tuple.class);
        Values expectedValues1 = mock(Values.class);
        Values expectedValues2 = mock(Values.class);
        Values expectedValues3 = mock(Values.class);

        OutputCollector outputCollector = mock(OutputCollector.class);
        // use this collector in some component then:

        // tuple acknowledgement
        assertThat(outputCollector).acked(expectedTuple1, expectedTuple2, expectedTuple3);
        assertThat(outputCollector).ackedExactly(expectedTuple1, expectedTuple2, expectedTuple3);
        assertThat(outputCollector).ackedOnly(expectedTuple1, expectedTuple2, expectedTuple3);

        assertThat(outputCollector).didNotAck(expectedTuple1, expectedTuple2, expectedTuple3);

        // values emission
        assertThat(outputCollector).emitted(expectedValues1, expectedValues2, expectedValues3);
        assertThat(outputCollector).emittedExactly(expectedValues1, expectedValues2, expectedValues3);
        assertThat(outputCollector).emittedOnly(expectedValues1, expectedValues2, expectedValues3);

        assertThat(outputCollector).didNotEmit(expectedValues1, expectedValues2, expectedValues3);

        // tuple failure
        assertThat(outputCollector).failed(expectedTuple1, expectedTuple2, expectedTuple3);
        assertThat(outputCollector).failedExactly(expectedTuple1, expectedTuple2, expectedTuple3);
        assertThat(outputCollector).failedOnly(expectedTuple1, expectedTuple2, expectedTuple3);

        assertThat(outputCollector).didNotFail(expectedTuple1, expectedTuple2, expectedTuple3);
    }

    public void document_TridentCollectorAssert() {
        Values expectedValues1 = mock(Values.class);
        Values expectedValues2 = mock(Values.class);
        Values expectedValues3 = mock(Values.class);

        TridentCollector tridentCollector = mock(TridentCollector.class);
        // use this collector in some component then:

        // values emission
        assertThat(tridentCollector).emitted(expectedValues1, expectedValues2, expectedValues3);
        assertThat(tridentCollector).emittedExactly(expectedValues1, expectedValues2, expectedValues3);
        assertThat(tridentCollector).emittedOnly(expectedValues1, expectedValues2, expectedValues3);

        assertThat(tridentCollector).didNotEmit(expectedValues1, expectedValues2, expectedValues3);
    }
}
