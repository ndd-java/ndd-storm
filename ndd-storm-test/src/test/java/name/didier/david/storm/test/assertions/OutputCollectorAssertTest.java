package name.didier.david.storm.test.assertions;

import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import backtype.storm.task.OutputCollector;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import name.didier.david.storm.test.utils.TupleMap;
import name.didier.david.test4j.testng.TestNgMockitoListener;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public class OutputCollectorAssertTest
        extends AbstractCollectorAssertTestCase {

    @Mock
    protected OutputCollector collector;

    private OutputCollectorAssert asserter;
    private OutputCollectorAssert asserterOnActual;

    @BeforeMethod
    public void create_asserter() {
        asserter = new OutputCollectorAssert(collector);
        asserterOnActual = new OutputCollectorAssert(new OutputCollector(collector));
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedTuples")
    public void acked_should_pass_when_tuples_are_acknowledged(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.ack(actualTuple);
        }
        asserter.acked(expectedTuples);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void acked_should_fail_when_the_collector_is_not_a_mock() {
        collector.ack(tuple);
        asserterOnActual.acked(tuple);
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedTuples")
    public void acked_should_fail_when_any_tuple_is_not_acknowledged(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.ack(actualTuple);
        }
        asserter.acked(expectedTuples);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedOnlyTuples")
    public void ackedOnly_should_pass_when_tuples_are_acknowledged(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.ack(actualTuple);
        }
        asserter.ackedOnly(expectedTuples);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void ackedOnly_should_fail_when_the_collector_is_not_a_mock() {
        collector.ack(tuple);
        asserterOnActual.ackedOnly(tuple);
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedOnlyTuples")
    public void ackedOnly_should_fail_when_any_tuple_is_not_acknowledged(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.ack(actualTuple);
        }
        asserter.ackedOnly(expectedTuples);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedExactlyTuples")
    public void ackedExactly_should_pass_when_tuples_are_acknowledged(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.ack(actualTuple);
        }
        asserter.ackedExactly(expectedTuples);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void ackedExactly_should_fail_when_the_collector_is_not_a_mock() {
        collector.ack(tuple);
        asserterOnActual.ackedExactly(tuple);
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedExactlyTuples")
    public void ackedExactly_should_fail_when_any_tuple_is_not_acknowledged(Tuple[] actualTuples,
            Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.ack(actualTuple);
        }
        asserter.ackedExactly(expectedTuples);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyNotContainedTuples")
    public void didNotAcked_should_pass_when_tuples_are_not_acknowledged(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.ack(actualTuple);
        }
        asserter.didNotAck(expectedTuples);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void didNotAcked_should_fail_when_the_collector_is_not_a_mock() {
        collector.ack(tuple);
        asserterOnActual.didNotAck(tuple);
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyNotContainedTuples")
    public void didNotAcked_should_fail_when_any_tuple_is_acknowledged(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.ack(actualTuple);
        }
        asserter.didNotAck(expectedTuples);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedTuples")
    public void failed_should_pass_when_tuple_are_failed(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.fail(actualTuple);
        }
        asserter.failed(expectedTuples);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void failed_should_fail_when_the_collector_is_not_a_mock() {
        collector.fail(tuple);
        asserterOnActual.failed(tuple);
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedTuples")
    public void failed_should_fail_when_any_tuple_is_not_failed(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.fail(actualTuple);
        }
        asserter.failed(expectedTuples);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedOnlyTuples")
    public void failedOnly_should_pass_when_tuples_are_failed(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.fail(actualTuple);
        }
        asserter.failedOnly(expectedTuples);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void failedOnly_should_fail_when_the_collector_is_not_a_mock() {
        collector.fail(tuple);
        asserterOnActual.failedOnly(tuple);
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedOnlyTuples")
    public void failedOnly_should_fail_when_any_tuple_is_not_failed(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.fail(actualTuple);
        }
        asserter.failedOnly(expectedTuples);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedExactlyTuples")
    public void failedExactly_should_pass_when_tuples_are_failed(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.fail(actualTuple);
        }
        asserter.failedExactly(expectedTuples);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void failedExactly_should_fail_when_the_collector_is_not_a_mock() {
        collector.fail(tuple);
        asserterOnActual.failedExactly(tuple);
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedExactlyTuples")
    public void failedExactly_should_fail_when_any_tuple_is_not_failed(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.fail(actualTuple);
        }
        asserter.failedExactly(expectedTuples);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyNotContainedTuples")
    public void didNotFailed_should_pass_when_tuples_are_not_failed(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.fail(actualTuple);
        }
        asserter.didNotFail(expectedTuples);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void didNotFailed_should_fail_when_the_collector_is_not_a_mock() {
        collector.fail(tuple);
        asserterOnActual.didNotFail(tuple);
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyNotContainedTuples")
    public void didNotFailed_should_fail_when_any_tuple_is_failed(Tuple[] actualTuples, Tuple[] expectedTuples) {
        for (Tuple actualTuple : actualTuples) {
            collector.fail(actualTuple);
        }
        asserter.didNotFail(expectedTuples);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedValues")
    public void emitted_should_pass_when_values_are_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emitted(expectedValues);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void emitted_should_fail_when_the_collector_is_not_a_mock() {
        collector.emit(new Values());
        asserterOnActual.emitted(new Values());
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedValues")
    public void emitted_should_fail_when_any_value_is_not_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emitted(expectedValues);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedOnlyValues")
    public void emittedOnly_should_pass_when_values_are_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emittedOnly(expectedValues);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void emittedOnly_should_fail_when_the_collector_is_not_a_mock() {
        collector.emit(new Values());
        asserterOnActual.emittedOnly(new Values());
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedOnlyValues")
    public void emittedOnly_should_fail_when_any_value_is_not_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emittedOnly(expectedValues);
    }

    // -----------------------------------------------------------------------------------------------------------------

    @Test(dataProvider = "successfullyContainedExactlyValues")
    public void emittedExactly_should_pass_when_values_are_emitted(Values[] actualValues, Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emittedExactly(expectedValues);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void emittedExactly_should_fail_when_the_collector_is_not_a_mock() {
        collector.emit(new Values());
        asserterOnActual.emittedExactly(new Values());
    }

    @Test(expectedExceptions = AssertionError.class, dataProvider = "unsuccessfullyContainedExactlyValues")
    public void emittedExactly_should_fail_when_any_value_is_not_emitted(Values[] actualValues,
            Values[] expectedValues) {
        for (Values actualValue : actualValues) {
            collector.emit(actualValue);
        }
        asserter.emittedExactly(expectedValues);
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void didNotEmit_should_pass_when_values_are_not_emitted() {
        collector.emit(new Values(1));
        collector.emit(anchor, new Values(2));
        collector.emit(anchors, new Values(3));
        collector.emit(STREAM_ID, new Values(4));
        collector.emit(STREAM_ID, anchor, new Values(5));
        collector.emit(STREAM_ID, anchors, new Values(6));

        asserter.didNotEmit(new Values(99));
    }

    @Test(expectedExceptions = AssertionError.class)
    public void didNotEmit_should_fail_when_the_collector_is_not_a_mock() {
        collector.emit(new Values());
        asserterOnActual.didNotEmit(new Values());
    }

    public void didNotEmit_should_fail_when_any_value_is_emitted() {
        collector.emit(new Values(1));
        collector.emit(anchor, new Values(2));
        collector.emit(anchors, new Values(3));
        collector.emit(STREAM_ID, new Values(4));
        collector.emit(STREAM_ID, anchor, new Values(5));
        collector.emit(STREAM_ID, anchors, new Values(6));

        for (int i = 1; i < 7; i++) {
            try {
                asserter.didNotEmit(new Values(i));
                failBecauseExceptionWasNotThrown(AssertionError.class);
            } catch (AssertionError e) {
                // expected
            }
        }

        try {
            asserter.didNotEmit(new Values(1), new Values(2), new Values(3), new Values(4), new Values(5),
                    new Values(6));
            failBecauseExceptionWasNotThrown(AssertionError.class);
        } catch (AssertionError e) {
            // expected
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** Contains, in any order. */
    @DataProvider(name = "successfullyContainedTuples")
    private Object[][] successfullyContainedTuples() {
        // @formatter:off
        TupleMap tm = new TupleMap();
        return new Tuple[][][] {
            { { tm.tuple(1)              }, { tm.tuple(1)              } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(1)              } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(1), tm.tuple(2) } },
            { { tm.tuple(2), tm.tuple(1) }, { tm.tuple(1), tm.tuple(2) } },
        };
        // @formatter:on
    }

    /** Does not contain, in any order. */
    @DataProvider(name = "unsuccessfullyContainedTuples")
    private Object[][] unsuccessfullyContainedTuples() {
        // @formatter:off
        TupleMap tm = new TupleMap();
        return new Tuple[][][] {
            { {             }, { tm.tuple(1)              } },
            { { tm.tuple(1) }, { tm.tuple(1), tm.tuple(2) } },
        };
        // @formatter:on
    }

    /** Contains only, in any order. */
    @DataProvider(name = "successfullyContainedOnlyTuples")
    private Object[][] successfullyContainedOnlyTuples() {
        // @formatter:off
        TupleMap tm = new TupleMap();
        return new Tuple[][][] {
            { { tm.tuple(1)              }, { tm.tuple(1)              } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(1), tm.tuple(2) } },
            { { tm.tuple(2), tm.tuple(1) }, { tm.tuple(1), tm.tuple(2) } },
        };
        // @formatter:on
    }

    /** Does not only contain, in any order. */
    @DataProvider(name = "unsuccessfullyContainedOnlyTuples")
    private Object[][] unsuccessfullyContainedOnlyTuples() {
        // @formatter:off
        TupleMap tm = new TupleMap();
        return new Tuple[][][] {
            { {                          }, { tm.tuple(1)              } },
            { { tm.tuple(1)              }, { tm.tuple(1), tm.tuple(2) } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(1)              } },
        };
        // @formatter:on
    }

    /** Contains exactly, in order. */
    @DataProvider(name = "successfullyContainedExactlyTuples")
    private Object[][] successfullyContainedExactlyTuples() {
        // @formatter:off
        TupleMap tm = new TupleMap();
        return new Tuple[][][] {
            { { tm.tuple(1)              }, { tm.tuple(1)              } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(1), tm.tuple(2) } },
        };
        // @formatter:on
    }

    /** Does not exactly contain, in order. */
    @DataProvider(name = "unsuccessfullyContainedExactlyTuples")
    private Object[][] unsuccessfullyContainedExactlyTuples() {
        // @formatter:off
        TupleMap tm = new TupleMap();
        return new Tuple[][][] {
            { {                          }, { tm.tuple(1)              } },
            { { tm.tuple(1)              }, { tm.tuple(1), tm.tuple(2) } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(1)              } },
            { { tm.tuple(2), tm.tuple(1) }, { tm.tuple(1), tm.tuple(2) } },
        };
        // @formatter:on
    }

    /** Does not contain, in any order. */
    @DataProvider(name = "successfullyNotContainedTuples")
    private Object[][] successfullyNotContainedTuples() {
        // @formatter:off
        TupleMap tm = new TupleMap();
        return new Tuple[][][] {
            { { tm.tuple(1)              }, { tm.tuple(2)              } },
            { { tm.tuple(1)              }, { tm.tuple(2), tm.tuple(3) } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(3)              } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(3), tm.tuple(4) } },
        };
        // @formatter:on
    }

    /** Contains, in any order. */
    @DataProvider(name = "unsuccessfullyNotContainedTuples")
    private Object[][] unsuccessfullyNotContainedTuples() {
        // @formatter:off
        TupleMap tm = new TupleMap();
        return new Tuple[][][] {
            { { tm.tuple(1)              }, { tm.tuple(1)              } },
            { { tm.tuple(1)              }, { tm.tuple(1), tm.tuple(2) } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(1)              } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(1), tm.tuple(2) } },
            { { tm.tuple(1), tm.tuple(2) }, { tm.tuple(2), tm.tuple(1) } },
        };
        // @formatter:on
    }
}
