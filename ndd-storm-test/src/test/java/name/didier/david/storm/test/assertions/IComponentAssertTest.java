package name.didier.david.storm.test.assertions;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.Test;

import backtype.storm.topology.IComponent;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseComponent;
import backtype.storm.tuple.Fields;

@Test(groups = UNIT)
public class IComponentAssertTest {

    private static final String FIELD_1 = "field_1";
    private static final String FIELD_2 = "field_2";

    private IComponentAssert asserter;

    public void declareExactly_should_pass_when_all_fields_are_matched() {
        asserter = new IComponentAssert(newComponent(FIELD_1));
        asserter.declareExactly(FIELD_1);
        asserter = new IComponentAssert(newComponent(FIELD_1, FIELD_2));
        asserter.declareExactly(FIELD_1, FIELD_2);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void declareExactly_should_fail_when_one_field_is_not_declared() {
        asserter = new IComponentAssert(newComponent(FIELD_1));
        asserter.declareExactly(FIELD_1, FIELD_2);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void declareExactly_should_fail_when_one_field_is_not_tested() {
        asserter = new IComponentAssert(newComponent(FIELD_1, FIELD_2));
        asserter.declareExactly(FIELD_1);
    }

    @Test(expectedExceptions = AssertionError.class)
    public void declareExactly_should_fail_when_fields_are_in_a_different_order() {
        asserter = new IComponentAssert(newComponent(FIELD_1, FIELD_2));
        asserter.declareExactly(FIELD_2, FIELD_1);
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static IComponent newComponent(String... fields) {
        return new IComponentImpl(fields);
    }

    private static class IComponentImpl
            extends BaseComponent {
        private static final long serialVersionUID = 1L;

        private final String[] fields;

        public IComponentImpl(String... fields) {
            super();
            this.fields = fields;
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
            declarer.declare(new Fields(fields));
        }
    }
}
