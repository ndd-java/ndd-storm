package name.didier.david.storm.test.assertions;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

import static name.didier.david.test4j.coverage.CoverageUtils.coverConstructor;
import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.Test;

import backtype.storm.task.OutputCollector;
import backtype.storm.topology.IComponent;
import storm.trident.operation.TridentCollector;

@Test(groups = UNIT)
public class StormAssertionsTest {

    public void stormAssertions() {
        coverConstructor(StormAssertions.class);
    }

    public void assertThatIComponent() {
        IComponentAssert asserter = StormAssertions.assertThat(mock(IComponent.class));
        assertThat(asserter).isNotNull();
    }

    public void assertThatOutputCollector() {
        OutputCollectorAssert asserter = StormAssertions.assertThat(mock(OutputCollector.class));
        assertThat(asserter).isNotNull();
    }

    public void assertThatTridentCollector() {
        TridentCollectorAssert asserter = StormAssertions.assertThat(mock(TridentCollector.class));
        assertThat(asserter).isNotNull();
    }
}
