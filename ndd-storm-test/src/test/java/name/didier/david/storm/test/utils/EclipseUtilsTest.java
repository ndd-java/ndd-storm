package name.didier.david.storm.test.utils;

import static org.assertj.core.api.Assertions.assertThat;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.Test;

import backtype.storm.LocalCluster;
import name.didier.david.test4j.assertions.CodingStandardAssertions;
import name.didier.david.test4j.coverage.CoverageUtils;

@Test(groups = UNIT)
public class EclipseUtilsTest {

    public void should_be_an_utility_class() {
        CodingStandardAssertions.assertThat(EclipseUtils.class).isUtility();
        CoverageUtils.coverConstructor(EclipseUtils.class);
    }

    public void newLocalCluster() {
        assertThat(EclipseUtils.newLocalCluster()).isInstanceOf(LocalCluster.class);
    }
}
