package name.didier.david.storm.test.assertions;

import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import java.util.List;

import org.mockito.Mock;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

@Test(groups = UNIT)
public abstract class AbstractCollectorAssertTestCase {

    protected static final String STREAM_ID = "STREAM_ID";

    @Mock
    protected Tuple anchor;
    @Mock
    protected List<Tuple> anchors;
    @Mock
    protected Tuple tuple;

    protected AbstractCollectorAssertTestCase() {
        super();
    }

    protected static Values v(Object... values) {
        return new Values(values);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** Contains, in any order. */
    @DataProvider(name = "successfullyContainedValues")
    protected Object[][] successfullyContainedValues() {
        // @formatter:off
        return new Values[][][] {
            { { v(1)       }, { v(1)       } },
            { { v(1), v(2) }, { v(1)       } },
            { { v(1), v(2) }, { v(1), v(2) } },
            { { v(2), v(1) }, { v(1), v(2) } },
        };
        // @formatter:on
    }

    /** Does not contain, in any order. */
    @DataProvider(name = "unsuccessfullyContainedValues")
    protected Object[][] unsuccessfullyContainedValues() {
        // @formatter:off
        return new Values[][][] {
            { {         }, { v(1)       } },
            { { v(1)    }, { v(1), v(2) } },
            { { v(1)    }, { v(1, 2)    } },
            { { v(1, 2) }, { v(2, 1)    } },
        };
        // @formatter:on
    }

    @DataProvider(name = "successfullyContainedOnlyValues")
    protected Object[][] successfullyContainedOnlyValues() {
        // @formatter:off
        return new Values[][][] {
            { { v(1)       }, { v(1)       } },
            { { v(1), v(2) }, { v(1), v(2) } },
            { { v(2), v(1) }, { v(1), v(2) } },
        };
        // @formatter:on
    }

    /** Does not only contain, in any order. */
    @DataProvider(name = "unsuccessfullyContainedOnlyValues")
    protected Object[][] unsuccessfullyContainedOnlyValues() {
        // @formatter:off
        return new Values[][][] {
            { {            }, { v(1)          } },
            { { v(1)       }, { v(1), v(2)    } },
            { { v(1), v(2) }, { v(1)          } },
            { { v(1)       }, { v(1, 2)       } },
            { { v(1, 2)    }, { v(2, 1)       } },
            { { v(1)       }, { v(1), v(1, 2) } },
            { { v(1, 2)    }, { v(1), v(2, 1) } },
        };
        // @formatter:on
    }

    /** Contains exactly, in order. */
    @DataProvider(name = "successfullyContainedExactlyValues")
    protected Object[][] successfullyContainedExactlyValues() {
        // @formatter:off
        return new Values[][][] {
            { { v(1)       }, { v(1)       } },
            { { v(1), v(2) }, { v(1), v(2) } },
        };
        // @formatter:on
    }

    /** Does not exactly contain, in order. */
    @DataProvider(name = "unsuccessfullyContainedExactlyValues")
    protected Object[][] unsuccessfullyContainedExactlyValues() {
        // @formatter:off
        return new Values[][][] {
            { {            }, { v(1)       } },
            { { v(1)       }, { v(1), v(2) } },
            { { v(1), v(2) }, { v(1)       } },
            { { v(1)       }, { v(1, 2)       } },
            { { v(1, 2)    }, { v(2, 1)       } },
            { { v(1)       }, { v(1), v(1, 2) } },
            { { v(1, 2)    }, { v(1), v(2, 1) } },
        };
        // @formatter:on
    }
}
