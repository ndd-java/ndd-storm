package name.didier.david.storm.test.spout;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.assertj.core.api.Fail.failBecauseExceptionWasNotThrown;

import static name.didier.david.storm.test.assertions.StormAssertions.assertThat;
import static name.didier.david.test4j.testng.TestNgDataProviders.NEGATIVE_NUMBERS;
import static name.didier.david.test4j.testng.TestNgGroups.UNIT;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import backtype.storm.Config;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import name.didier.david.test4j.testng.TestNgDataProviders;
import name.didier.david.test4j.testng.TestNgMockitoListener;

@Test(groups = UNIT)
@Listeners(TestNgMockitoListener.class)
public class IterableBatchSpoutTest
        extends AbstractBatchSpoutTestCase {

    // -----------------------------------------------------------------------------------------------------------------

    public void constructor_should_reject_null_fields() {
        try {
            spout(null, DATA, BATCH_SIZE);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("fields");
        }
    }

    public void constructor_should_reject_null_data() {
        try {
            spout(FIELDS, null, BATCH_SIZE);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("data");
        }
    }

    @Test(dataProviderClass = TestNgDataProviders.class, dataProvider = NEGATIVE_NUMBERS)
    public void constructor_should_reject_negative_batch_size(int batchSize) {
        try {
            spout(FIELDS, DATA, batchSize);
            failBecauseExceptionWasNotThrown(IllegalArgumentException.class);
        } catch (IllegalArgumentException e) {
            assertThat(e).hasMessageContaining("batchSize");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void should_emit_one_batch_of_size_smaller_than_data() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE - 1);
        spout.emitBatch(1, collector);
        assertThat(collector).emittedExactly(values(1, 2));
    }

    public void should_emit_one_batch_of_size_greater_than_data() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE + 1);
        spout.emitBatch(1, collector);
        assertThat(collector).emittedExactly(values(1, 2, 3));
    }

    public void should_emit_twice_a_batch_of_size_smaller_than_data() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE - 1);
        spout.emitBatch(1, collector);
        spout.emitBatch(1, collector);
        assertThat(collector).emittedExactly(values(1, 2, 1, 2));
    }

    public void should_emit_twice_a_batch_of_size_greater_than_data() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE + 1);
        spout.emitBatch(1, collector);
        spout.emitBatch(1, collector);
        assertThat(collector).emittedExactly(values(1, 2, 3, 1, 2, 3));
    }

    public void should_emit_two_batches_of_size_smaller_than_data() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE - 1);
        spout.emitBatch(1, collector);
        spout.emitBatch(2, collector);
        assertThat(collector).emittedExactly(values(1, 2, 3));
    }

    public void should_emit_two_batches_of_size_greater_than_data() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE + 1);
        spout.emitBatch(1, collector);
        spout.emitBatch(2, collector);
        assertThat(collector).emittedExactly(values(1, 2, 3));
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void ack_should_remove_the_given_batch() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE - 1);
        spout.emitBatch(1, collector);
        spout.ack(1);
        // TODO ack_should_remove_the_given_batch
    }

    // public void ack_should_not_prevent_a_batch_replay() {
    // needed?
    // }

    // -----------------------------------------------------------------------------------------------------------------

    public void close_should_prevent_any_further_batch() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE - 1);
        spout.close();

        try {
            spout.emitBatch(1, collector);
            failBecauseExceptionWasNotThrown(IllegalStateException.class);
        } catch (IllegalStateException e) {
            assertThat(e).hasMessage("Spout is closed");
        }
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void componentConfiguration_should_be_readable() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE - 1);
        assertThat(spout.getComponentConfiguration()).containsExactly(entry(Config.TOPOLOGY_MAX_TASK_PARALLELISM, 1));
    }

    public void componentConfiguration_should_be_lazy_initialized() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE - 1);
        assertThat(spout.getComponentConfiguration()).isSameAs(spout.getComponentConfiguration());
    }

    // -----------------------------------------------------------------------------------------------------------------

    public void outputFields_should_be_readable() {
        IterableBatchSpout spout = openedSpout(FIELDS, DATA, DATA_SIZE - 1);
        assertThat(spout.getOutputFields()).isEqualTo(FIELDS);
    }

    // -----------------------------------------------------------------------------------------------------------------

    private static IterableBatchSpout spout(Fields fields, Iterable<Values> data, int batchSize) {
        return new IterableBatchSpout(fields, data, batchSize);
    }

    private static IterableBatchSpout openedSpout(Fields fields, Iterable<Values> data, int batchSize) {
        IterableBatchSpout spout = spout(fields, data, batchSize);
        spout.open(null, null);
        return spout;
    }
}
