package name.didier.david.storm.test.utils;

import org.yaml.snakeyaml.Yaml;

import backtype.storm.LocalCluster;

/**
 * Utilities dealing with Eclipse.
 *
 * @author ddidier
 */
public final class EclipseUtils {

    /** Utility class. */
    private EclipseUtils() {
        super();
    }

    /**
     * Some dependencies are embedded inside the Eclipse TestNG JAR and may (will) conflict with some project
     * dependencies. A workaround is to open the project properties then TestNG and check 'Use project TestNG JAR'. The
     * conflicting JAR are so far : 'SnakeYAML'.
     *
     * @return a new {@link LocalCluster}
     */
    public static LocalCluster newLocalCluster() {
        try {
            return new LocalCluster();
        } catch (NoSuchMethodError e) {
            if (e.getMessage().contains(Yaml.class.getName())) {
                throw new AssertionError("TestNG Eclipse plugin conflict... "
                        + "Please check 'Use project TestNG JAR' in 'Project properties > TestNG'", e);
            }
            throw e;
        }
    }
}
