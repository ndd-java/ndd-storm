package name.didier.david.storm.test.assertions;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.assertj.core.api.ObjectEnumerableAssert;
import org.mockito.ArgumentCaptor;

import backtype.storm.topology.IComponent;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;

/**
 * Assertion methods for {@link IComponent}s. To create an instance of this class, invoke
 * {@link StormAssertions#assertThat(IComponent)}.
 * 
 * @author ddidier
 */
public class IComponentAssert
        extends AbstractStormAssert<IComponentAssert, IComponent> {

    /**
     * To create an instance of this class, invoke {@link StormAssertions#assertThat(IComponent)}.
     * 
     * @param actual the object under test.
     */
    protected IComponentAssert(IComponent actual) {
        super(actual, IComponentAssert.class);
    }

    /**
     * Verifies that the actual component declares only the given values and nothing else, <b>in order</b>.
     * 
     * @param fields the given fields.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsExactly(Object...)
     */
    public IComponentAssert declareExactly(String... fields) {
        OutputFieldsDeclarer declarer = mock(OutputFieldsDeclarer.class);
        actual.declareOutputFields(declarer);

        ArgumentCaptor<Fields> fieldsCaptor = ArgumentCaptor.forClass(Fields.class);
        verify(declarer).declare(fieldsCaptor.capture());

        assertThat(fieldsCaptor.getValue()).containsExactly(fields);

        return myself;
    }
}
