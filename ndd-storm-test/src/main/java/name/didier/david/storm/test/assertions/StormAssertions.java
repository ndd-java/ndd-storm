package name.didier.david.storm.test.assertions;

import backtype.storm.task.OutputCollector;
import backtype.storm.topology.IComponent;
import storm.trident.operation.TridentCollector;

/**
 * Entry point for assertion methods for different Storm data types.
 * 
 * @author ddidier
 */
public class StormAssertions {

    /**
     * Utility class.
     */
    protected StormAssertions() {
        super();
    }

    /**
     * Creates a new instance of <code>{@link IComponentAssert}</code>.
     * 
     * @param actual the actual value.
     * @return the created assertion object.
     */
    public static IComponentAssert assertThat(IComponent actual) {
        return new IComponentAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link OutputCollectorAssert}</code>.
     * 
     * @param actual the actual value.
     * @return the created assertion object.
     */
    public static OutputCollectorAssert assertThat(OutputCollector actual) {
        return new OutputCollectorAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link TridentCollectorAssert}</code>.
     * 
     * @param actual the actual value.
     * @return the created assertion object.
     */
    public static TridentCollectorAssert assertThat(TridentCollector actual) {
        return new TridentCollectorAssert(actual);
    }
}
