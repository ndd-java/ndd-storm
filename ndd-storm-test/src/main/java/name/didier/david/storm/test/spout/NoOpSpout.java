package name.didier.david.storm.test.spout;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotEmpty;

import java.util.Map;

import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.tuple.Fields;

/**
 * A spout that emit nothing.
 *
 * @author ddidier
 */
public class NoOpSpout
        extends BaseRichSpout {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The declared output fields. */
    private final String[] outputFields;

    /**
     * @param outputFields the declared output fields.
     */
    public NoOpSpout(String... outputFields) {
        super();
        this.outputFields = checkNotEmpty(outputFields, "outputFields");
    }

    // private final String streamId;
    //
    // public NoOpSpout(String streamId, String... outputFields) {
    // super();
    // this.streamId = checkNotBlank(streamId, "streamId");
    // this.outputFields = checkNotEmpty(outputFields, "outputFields");
    // }
    //
    // public NoOpSpout(String streamId, String... outputFields) {
    // super();
    // this.streamId = checkNotBlank(streamId, "streamId");
    // this.outputFields = checkNotEmpty(outputFields, "outputFields");
    // }

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        // empty
    }

    @Override
    public void nextTuple() {
        // empty
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields(outputFields));
        // declarer.declareStream(streamId, new Fields(outputFields));
    }
}
