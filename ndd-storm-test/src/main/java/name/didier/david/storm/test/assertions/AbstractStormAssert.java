package name.didier.david.storm.test.assertions;

import static java.lang.String.format;

import org.assertj.core.api.AbstractAssert;
import org.assertj.core.internal.Failures;
import org.mockito.internal.util.MockUtil;

/**
 * Base class for all Storm assertions.
 *
 * @author ddidier
 * 
 * @param <S> the "self" type of this assertion class.
 * @param <A> the type of the "actual" value.
 */
public abstract class AbstractStormAssert<S extends AbstractAssert<S, A>, A>
        extends AbstractAssert<S, A> {

    /**
     * Abstract constructor.
     *
     * @param actual the actual object under test.
     * @param selfType the type of the object under test.
     */
    protected AbstractStormAssert(A actual, Class<?> selfType) {
        super(actual, selfType);
    }

    /**
     * Verify that the given object is a mock.
     *
     * @return the given object.
     */
    protected S isMock() {
        if (new MockUtil().isMock(actual)) {
            return myself;
        }
        throw failure("Expecting %s to be a mock", actual.getClass());
    }

    /**
     * Creates a <code>{@link AssertionError}</code> using the given formated {@code String} as message.
     *
     * @param format the format string of the message.
     * @param arguments referenced by the format specifiers in the format string.
     * @return the created <code>{@link AssertionError}</code>.
     */
    protected AssertionError failure(String format, Object... arguments) {
        return Failures.instance().failure(format(format, arguments));
    }
}
