package name.didier.david.storm.test.utils;

import static org.mockito.Mockito.mock;

import java.util.Hashtable;
import java.util.Map;

import backtype.storm.tuple.Tuple;

/**
 * Mocked tuples indexed by an integer.
 * 
 * @author ddidier
 */
public class TupleMap {

    /** Mocked tuples indexed by an integer. */
    private final Map<Integer, Tuple> tuples = new Hashtable<>();

    /** Default constructor. */
    public TupleMap() {
        super();
    }

    /**
     * @param key the tuple index.
     * @return a mocked tuple indexed by an integer.
     */
    public Tuple tuple(int key) {
        Tuple tuple = tuples.get(key);

        if (tuple == null) {
            tuple = mock(Tuple.class);
            tuples.put(key, tuple);
        }

        return tuple;
    }

    /**
     * An alias for {@link #tuple(int)}.
     * 
     * @param key the tuple index.
     * @return a mocked tuple indexed by an integer.
     */
    public Tuple t(int key) {
        return tuple(key);
    }
}
