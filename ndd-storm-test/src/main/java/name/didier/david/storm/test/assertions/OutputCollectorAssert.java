package name.didier.david.storm.test.assertions;

import static java.lang.Integer.MAX_VALUE;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyCollection;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.assertj.core.api.ObjectEnumerableAssert;
import org.mockito.ArgumentCaptor;

import backtype.storm.task.OutputCollector;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

/**
 * Assertion methods for {@link OutputCollector}s. To create an instance of this class, invoke
 * {@link StormAssertions#assertThat(OutputCollector)}.
 *
 * @author ddidier
 */
public class OutputCollectorAssert
        extends AbstractStormAssert<OutputCollectorAssert, OutputCollector> {

    /**
     * To create an instance of this class, invoke {@link StormAssertions#assertThat(OutputCollector)}.
     *
     * @param actual the object under test.
     */
    protected OutputCollectorAssert(OutputCollector actual) {
        super(actual, OutputCollectorAssert.class);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Verifies that the collector has acknowledged all the given tuples in any order. The actual collector must be a
     * mock.
     *
     * @param expectedTuples the tuples expected to have been acknowledged.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#contains(Object...)
     */
    public OutputCollectorAssert acked(Tuple... expectedTuples) {
        assertThat(actualAckedTuples()).contains(expectedTuples);
        return myself;
    }

    /**
     * Verifies that the collector has acknowledged only the given tuples and nothing else in any order. The actual
     * collector must be a mock.
     *
     * @param expectedTuples the tuples expected to have been acknowledged.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsOnly(Object...)
     */
    public OutputCollectorAssert ackedOnly(Tuple... expectedTuples) {
        assertThat(actualAckedTuples()).containsOnly(expectedTuples);
        return myself;
    }

    /**
     * Verifies that the collector has acknowledged only the given tuples and nothing else in order. The actual
     * collector must be a mock.
     *
     * @param expectedTuples the tuples expected to have been acknowledged.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsExactly(Object...)
     */
    public OutputCollectorAssert ackedExactly(Tuple... expectedTuples) {
        assertThat(actualAckedTuples()).containsExactly(expectedTuples);
        return myself;
    }

    /**
     * Verifies that the collector has not acknowledged any of the given tuples in any order. The actual collector must
     * be a mock.
     *
     * @param expectedTuples the tuples expected to not have been acknowledged.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#doesNotContain(Object...)
     */
    public OutputCollectorAssert didNotAck(Tuple... expectedTuples) {
        assertThat(actualAckedTuples()).doesNotContain(expectedTuples);
        return myself;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Verifies that the collector has failed all the given tuples in any order. The actual collector must be a mock.
     *
     * @param expectedTuples the tuple expected to have been failed.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#contains(Object...)
     */
    public OutputCollectorAssert failed(Tuple... expectedTuples) {
        assertThat(actualFailedTuples()).contains(expectedTuples);
        return myself;
    }

    /**
     * Verifies that the collector has failed only the given tuples and nothing else in any order. The actual collector
     * must be a mock.
     *
     * @param expectedTuples the tuple expected to have been failed.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsOnly(Object...)
     */
    public OutputCollectorAssert failedOnly(Tuple... expectedTuples) {
        assertThat(actualFailedTuples()).containsOnly(expectedTuples);
        return myself;
    }

    /**
     * Verifies that the collector has failed only the given tuples and nothing else in order. The actual collector must
     * be a mock.
     *
     * @param expectedTuples the tuple expected to have been failed.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsExactly(Object...)
     */
    public OutputCollectorAssert failedExactly(Tuple... expectedTuples) {
        assertThat(actualFailedTuples()).containsExactly(expectedTuples);
        return myself;
    }

    /**
     * Verifies that the collector has not failed any of the given tuples in any order. The actual collector must be a
     * mock.
     *
     * @param expectedTuples the tuple expected to not have been failed.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#doesNotContain(Object...)
     */
    public OutputCollectorAssert didNotFail(Tuple... expectedTuples) {
        assertThat(actualFailedTuples()).doesNotContain(expectedTuples);
        return myself;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Verifies that the collector has emitted all the given values in any order, without anchor. The actual collector
     * must be a mock.
     *
     * @param expectedValues the values expected to have been emitted.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#contains(Object...)
     */
    public OutputCollectorAssert emitted(Values... expectedValues) {
        assertThat(actualEmittedValues()).contains(expectedValues);
        return myself;
    }

    /**
     * Verifies that the collector has emitted only the given values in any order, without anchor. The actual collector
     * must be a mock.
     *
     * @param expectedValues the values expected to have been emitted.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsOnly(Object...)
     */
    public OutputCollectorAssert emittedOnly(Values... expectedValues) {
        assertThat(actualEmittedValues()).containsOnly(expectedValues);
        return myself;
    }

    /**
     * Verifies that the collector has emitted only the given values and nothing else, in order, without anchor. The
     * actual collector must be a mock.
     *
     * @param expectedValues the values expected to have been emitted.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsExactly(Object...)
     */
    public OutputCollectorAssert emittedExactly(Values... expectedValues) {
        assertThat(actualEmittedValues()).containsExactly(expectedValues);
        return myself;
    }

    // TODO with anchor : warning one anchors list for one tuple !

    /**
     * Verifies that the collector has not emitted any of the given values, with or without anchor.
     *
     * @param expectedValues the values expected to have not been emitted.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#doesNotContain(Object...)
     */
    @SuppressWarnings("rawtypes")
    public OutputCollectorAssert didNotEmit(Values... expectedValues) {
        isMock();

        Map<String, ArgumentCaptor<List>> vc = new Hashtable<>();
        vc.put("Tuple", ArgumentCaptor.forClass(List.class));
        vc.put("AnchorTuple", ArgumentCaptor.forClass(List.class));
        vc.put("AnchorsTuple", ArgumentCaptor.forClass(List.class));
        vc.put("StreamTuple", ArgumentCaptor.forClass(List.class));
        vc.put("StreamAnchorTuple", ArgumentCaptor.forClass(List.class));
        vc.put("StreamAnchorsTuple", ArgumentCaptor.forClass(List.class));

        verify(actual, atMost(MAX_VALUE)).emit(vc.get("Tuple").capture());
        verify(actual, atMost(MAX_VALUE)).emit(any(Tuple.class), vc.get("AnchorTuple").capture());
        verify(actual, atMost(MAX_VALUE)).emit(anyCollection(), vc.get("AnchorsTuple").capture());
        verify(actual, atMost(MAX_VALUE)).emit(anyString(), vc.get("StreamTuple").capture());
        verify(actual, atMost(MAX_VALUE)).emit(anyString(), any(Tuple.class), vc.get("StreamAnchorTuple").capture());
        verify(actual, atMost(MAX_VALUE)).emit(anyString(), anyCollection(), vc.get("StreamAnchorsTuple").capture());

        for (Entry<String, ArgumentCaptor<List>> entry : vc.entrySet()) {
            List<List> actualValues = entry.getValue().getAllValues();
            if (!actualValues.isEmpty()) {
                assertThat(actualValues).doesNotContain(expectedValues);
            }
        }

        return myself;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the tuples actually acknowledged by the collector. */
    private List<Tuple> actualAckedTuples() {
        isMock();
        ArgumentCaptor<Tuple> tuplesCaptor = ArgumentCaptor.forClass(Tuple.class);
        verify(actual, atMost(MAX_VALUE)).ack(tuplesCaptor.capture());
        return tuplesCaptor.getAllValues();
    }

    /** @return the tuples actually failed by the collector. */
    private List<Tuple> actualFailedTuples() {
        isMock();
        ArgumentCaptor<Tuple> tuplesCaptor = ArgumentCaptor.forClass(Tuple.class);
        verify(actual, atMost(MAX_VALUE)).fail(tuplesCaptor.capture());
        return tuplesCaptor.getAllValues();
    }

    /** @return the values actually emitted by the collector. */
    @SuppressWarnings("rawtypes")
    private List<List> actualEmittedValues() {
        isMock();
        ArgumentCaptor<List> valuesCaptor = ArgumentCaptor.forClass(List.class);
        verify(actual, atMost(MAX_VALUE)).emit(valuesCaptor.capture());
        return valuesCaptor.getAllValues();
    }
}
