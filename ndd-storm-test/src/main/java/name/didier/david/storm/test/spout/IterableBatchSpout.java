package name.didier.david.storm.test.spout;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;
import static name.didier.david.check4j.api.ConciseCheckers.checkStrictlyPositive;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import backtype.storm.Config;
import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.IBatchSpout;

/**
 * A spout iterating on a specified data set only once.
 *
 * @author ddidier
 */
public class IterableBatchSpout
        implements IBatchSpout {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The fields emitted by the spout. */
    private final Fields fields;
    /** The data emitted by the spout. */
    private final Iterable<Values> data;
    /** The size of a single batch emitted by the spout. */
    private final int batchSize;

    /** An cycling iterator on data. */
    private transient Iterator<Values> dataIt;
    /** The emitted batches. */
    private transient Map<Long, List<Values>> batches = new HashMap<>();
    /** The spout configuration. */
    private transient Map<String, Object> configuration;

    /**
     * @param fields the fields emitted by the spout.
     * @param data the data emitted by the spout.
     * @param batchSize the size of a single batch emitted by the spout.
     */
    public IterableBatchSpout(Fields fields, Iterable<Values> data, int batchSize) {
        super();
        this.fields = checkNotNull(fields, "fields");
        this.data = checkNotNull(data, "data");
        this.batchSize = checkStrictlyPositive(batchSize, "batchSize");
    }

    @Override
    public void open(Map conf, TopologyContext context) {
        batches = new HashMap<>();
        dataIt = data.iterator();
    }

    @Override
    public void emitBatch(long batchId, TridentCollector collector) {
        if (isClosed()) {
            throw new IllegalStateException("Spout is closed");
        }

        List<Values> batch = batches.get(batchId);

        if (batch == null) {
            batch = new ArrayList<>();

            for (int i = 0; i < batchSize; i++) {
                if (dataIt.hasNext()) {
                    batch.add(dataIt.next());
                }
            }

            batches.put(batchId, batch);
        }

        for (List<Object> values : batch) {
            collector.emit(values);
        }
    }

    @Override
    public void ack(long batchId) {
        batches.remove(batchId);
    }

    @Override
    public void close() {
        batches = null;
        dataIt = null;
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        if (configuration == null) {
            configuration = createComponentConfiguration();
        }
        return configuration;
    }

    @Override
    public Fields getOutputFields() {
        return fields;
    }

    /**
     * @return a new component configuration used by {@link #getComponentConfiguration()}.
     */
    protected Map<String, Object> createComponentConfiguration() {
        Config newConfiguration = new Config();
        newConfiguration.setMaxTaskParallelism(1);
        return newConfiguration;
    }

    /**
     * @return <code>true</code> if the spout is closed, <code>false</code> otherwise.
     */
    protected boolean isClosed() {
        return batches == null;
    }
}
