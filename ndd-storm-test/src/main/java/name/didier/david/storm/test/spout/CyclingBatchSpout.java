package name.didier.david.storm.test.spout;

import static name.didier.david.check4j.api.ConciseCheckers.checkNotNull;
import static name.didier.david.check4j.api.ConciseCheckers.checkStrictlyPositive;

import java.io.Serializable;
import java.util.Iterator;
import java.util.Map;

import com.google.common.collect.Iterables;

import backtype.storm.task.TopologyContext;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import storm.trident.operation.TridentCollector;
import storm.trident.spout.IBatchSpout;

/**
 * A spout iterating on a specified data set the specified number of times.
 *
 * @author ddidier
 */
public class CyclingBatchSpout
        implements IBatchSpout {

    /** The serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The underlying spout using the given cycling iterator. */
    private final IterableBatchSpout delegate;

    /**
     * Cycle over the given data the specified number of times.
     *
     * @param fields the fields emitted by the spout.
     * @param data the data emitted by the spout.
     * @param batchSize the size of a single batch emitted by the spout.
     * @param batchCount the number of batches to emit.
     */
    public CyclingBatchSpout(Fields fields, Iterable<Values> data, int batchSize, int batchCount) {
        super();
        checkNotNull(data, "data");
        checkStrictlyPositive(batchCount, "batchCount");
        this.delegate = new IterableBatchSpout(fields, new CyclingIterable<>(data, batchCount), batchSize);
    }

    @Override
    public void open(Map conf, TopologyContext context) {
        delegate.open(conf, context);
    }

    @Override
    public void emitBatch(long batchId, TridentCollector collector) {
        delegate.emitBatch(batchId, collector);
    }

    @Override
    public void ack(long batchId) {
        delegate.ack(batchId);
    }

    @Override
    public void close() {
        delegate.close();
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return delegate.getComponentConfiguration();
    }

    @Override
    public Fields getOutputFields() {
        return delegate.getOutputFields();
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * A <b>serializable</b> {@link Iterable} which cycles through data the specified number of times.
     *
     * @author ddidier
     *
     * @param <T> the data type.
     */
    private static class CyclingIterable<T>
            implements Iterable<T>, Serializable {

        /** The serial version UID. */
        private static final long serialVersionUID = 1L;

        /** The data to iterate upon. */
        private final Iterable<T> data;
        /** The number of times to iterate. */
        private final Integer times;

        /**
         * @param data the data to iterate upon.
         * @param times the number of times to iterate.
         */
        public CyclingIterable(Iterable<T> data, int times) {
            super();
            this.data = checkNotNull(data, "data");
            this.times = checkStrictlyPositive(times, "times");
        }

        @Override
        public Iterator<T> iterator() {
            @SuppressWarnings("unchecked")
            Iterable<T>[] iterables = new Iterable[times];
            for (int i = 0; i < iterables.length; i++) {
                iterables[i] = data;
            }
            return Iterables.concat(iterables).iterator();
        }

    }
}
