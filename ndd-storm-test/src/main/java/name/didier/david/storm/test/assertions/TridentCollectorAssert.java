package name.didier.david.storm.test.assertions;

import static java.lang.Integer.MAX_VALUE;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.atMost;
import static org.mockito.Mockito.verify;

import java.util.List;

import org.assertj.core.api.ObjectEnumerableAssert;
import org.mockito.ArgumentCaptor;

import backtype.storm.tuple.Values;
import storm.trident.operation.TridentCollector;

/**
 * Assertion methods for {@link TridentCollector}s. To create an instance of this class, invoke
 * {@link StormAssertions#assertThat(TridentCollector)}.
 *
 * @author ddidier
 */
public class TridentCollectorAssert
        extends AbstractStormAssert<TridentCollectorAssert, TridentCollector> {

    /**
     * To create an instance of this class, invoke {@link StormAssertions#assertThat(TridentCollector)}.
     *
     * @param actual the object under test.
     */
    protected TridentCollectorAssert(TridentCollector actual) {
        super(actual, TridentCollectorAssert.class);
    }

    // -----------------------------------------------------------------------------------------------------------------

    /**
     * Verifies that the collector has emitted all the given values in any order, without anchor. The actual collector
     * must be a mock.
     *
     * @param expectedValues the values expected to have been emitted.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#contains(Object...)
     */
    public TridentCollectorAssert emitted(Values... expectedValues) {
        assertThat(actualEmittedValues()).contains(expectedValues);
        return myself;
    }

    /**
     * Verifies that the collector has emitted only the given values in any order, without anchor. The actual collector
     * must be a mock.
     *
     * @param expectedValues the values expected to have been emitted.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsOnly(Object...)
     */
    public TridentCollectorAssert emittedOnly(Values... expectedValues) {
        assertThat(actualEmittedValues()).containsOnly(expectedValues);
        return myself;
    }

    /**
     * Verifies that the collector has emitted only the given values and nothing else, in order, without anchor. The
     * actual collector must be a mock.
     *
     * @param expectedValues the values expected to have been emitted.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#containsExactly(Object...)
     */
    public TridentCollectorAssert emittedExactly(Values... expectedValues) {
        assertThat(actualEmittedValues()).containsExactly(expectedValues);
        return myself;
    }

    /**
     * Verifies that the collector has not emitted any of the given values.
     *
     * @param expectedValues the values expected to have not been emitted.
     * @return {@code this} assertion object.
     * @see ObjectEnumerableAssert#doesNotContain(Object...)
     */
    public TridentCollectorAssert didNotEmit(Values... expectedValues) {
        assertThat(actualEmittedValues()).doesNotContain(expectedValues);
        return myself;
    }

    // -----------------------------------------------------------------------------------------------------------------

    /** @return the values actually emitted by the collector. */
    @SuppressWarnings("rawtypes")
    private List<List> actualEmittedValues() {
        isMock();
        ArgumentCaptor<List> valuesCaptor = ArgumentCaptor.forClass(List.class);
        verify(actual, atMost(MAX_VALUE)).emit(valuesCaptor.capture());
        return valuesCaptor.getAllValues();
    }
}
